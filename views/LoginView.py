from tkinter import *
from PIL import ImageTk, Image


class LoginView:
    # Konstruktor
    def __init__(self):
        # Login
        self.login_win = Tk()
        self.login_win.resizable(False, False)
        self.login_win.geometry("600x400")
        self.login_win.title("Instruction Network - Login")
        self.login_win.iconbitmap("../images/instruction_network_icon.ico")
        self.background_image = ImageTk.PhotoImage(Image.open("../images/background.jpg").resize((600, 400)))
        self.background = Label(image=self.background_image)
        self.background.pack(fill="both", expand=True)

        self.top_level_wins = []

        # Frame - podaci za login
        self.info_frame = Frame(self.background, padx=20, pady=20, bg="#0F4C81")
        self.info_frame.grid(row=0, column=0)
        self.info_frame.place(relx=.25, rely=.3)

        # Username
        self.username_label = Label(self.info_frame, text="username: ", bg="#0F4C81", font=("Courier", 12))
        self.username_label.grid(row=0, column=0, padx=(15, 0))
        self.username_entry = Entry(self.info_frame)
        self.username_entry.grid(row=0, column=1, padx=(0, 15))

        # Password
        self.password_label = Label(self.info_frame, text="password: ", bg="#0F4C81", font=("Courier", 12))
        self.password_label.grid(row=1, column=0, padx=(15, 0))
        self.password_entry = Entry(self.info_frame, show="*")
        self.password_entry.grid(row=1, column=1, padx=(0, 15))

        # Button - Login
        self.login_button = Button(self.info_frame, text="Login", font=("Courier", 12), bg="#658DC6")
        self.login_button.grid(row=2, column=0, columnspan=2, sticky=W + E, padx=(15, 15), pady=(20, 0))

        # Button - Registracija
        self.register_button = Button(self.background, text="Registracija", bg="#0F4C81",
                                      font=("Courier", 8))
        self.register_button.grid(row=1, column=1)
        self.register_button.place(relx=.425, rely=.7)

    # Otvaranje novog TopLevel-a
    def add_top_level_win(self, win):
        self.top_level_wins.append(win)

    # Vraca sve TopLevel-e
    def get_top_level_wins(self):
        return self.top_level_wins

    # Prikaz views-a
    def run(self) -> None:
        self.login_win.mainloop()

