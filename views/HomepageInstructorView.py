from tkinter import *
from PIL import ImageTk, Image
from tkinter import messagebox


class HomepageInstructorView:
    # Konstruktor
    def __init__(self, view, root, instructor):
        self.homepage_win = Toplevel(root)
        self.homepage_win.protocol("WM_DELETE_WINDOW", lambda: self.on_closing_homepage(view, root))
        self.homepage_win.resizable(False, False)
        self.homepage_win.geometry("550x500")
        self.homepage_win.title("Instruction Network - Početna Stranica")
        self.homepage_win.iconbitmap("../images/instruction_network_icon.ico")
        self.homepage_win.configure(bg="#0F4C81")

        # Frame - Buttons
        self.buttons_frame = Frame(self.homepage_win)
        self.buttons_frame.grid(row=0, column=0, pady=50)
        self.buttons_frame.place(relx=.01, rely=.3)

        # Button - Pregled kreiranih termina
        self.view_appointments_button = Button(self.buttons_frame, text="PREGLED TERMINA", font=("Courier", 12),
                                               width=20, bg="#658DC6", borderwidth=.5)
        self.view_appointments_button.grid(row=0, column=0)

        # Button - Pregled rezerviranih termina
        self.view_reservations_button = Button(self.buttons_frame, text="PREGLED REZERVACIJA", font=("Courier", 12),
                                               width=20, bg="#658DC6", borderwidth=.5)
        self.view_reservations_button.grid(row=1, column=0)

        # Button - Kreiranje termina
        self.create_appointment_button = Button(self.buttons_frame, text="KREIRAJ TERMIN", font=("Courier", 12),
                                                width=20, bg="#658DC6", borderwidth=.5)
        self.create_appointment_button.grid(row=2, column=0)

        # Button - Uredi profil
        self.edit_account_button = Button(self.buttons_frame, text="UREDI PROFIL", font=("Courier", 12), width=20,
                                          bg="#658DC6", borderwidth=.5)
        self.edit_account_button.grid(row=3, column=0)

        # Button - Obriši profil
        self.delete_account_button = Button(self.buttons_frame, text="OBRIŠI RAČUN", font=("Courier", 12), width=20,
                                            bg="#658DC6", borderwidth=.5)
        self.delete_account_button.grid(row=4, column=0)

        # Button - Logout
        self.logout_button = Button(self.buttons_frame, text="LOG OUT", font=("Courier", 12), width=20,
                                    bg="#658DC6", borderwidth=.5, command=lambda: self.on_closing_homepage(view, root))
        self.logout_button.grid(row=5, column=0)

        # Frame - Sadržaj
        self.content_frame = Frame(self.homepage_win, bg="#0F4C81")
        self.content_frame.grid(row=0, column=1, padx=(5, 5))
        self.content_frame.place(relx=.5)

        # Slika korisnika
        self.user_image = ImageTk.PhotoImage(Image.open("../images/user_icon.png").resize((150, 150)))
        if instructor.get_image() is not None:
            with open("../images/tmp.jpg", "wb") as img:
                img.write(instructor.get_image())
            self.user_image = ImageTk.PhotoImage(Image.open("../images/tmp.jpg").resize((150, 150)))
        self.image = Label(self.content_frame, image=self.user_image)
        self.image.grid(row=0, column=0, columnspan=2, pady=(20, 20))

        # Ime
        self.first_name = Label(self.content_frame, text="Ime: " + instructor.get_first_name(), bg="#0F4C81",
                                font=("Courier", 12))
        self.first_name.grid(row=1, column=0, sticky=W)

        # Prezime
        self.last_name = Label(self.content_frame, text="Prezime: " + instructor.get_last_name(), bg="#0F4C81",
                               font=("Courier", 12))
        self.last_name.grid(row=2, column=0, sticky=W)

        # Email
        self.email = Label(self.content_frame, text="Email: " + instructor.get_email(), bg="#0F4C81",
                           font=("Courier", 12))
        self.email.grid(row=3, column=0, sticky=W)

        # Username
        self.username = Label(self.content_frame, text="Username: " + instructor.get_username(), bg="#0F4C81",
                              font=("Courier", 12))
        self.username.grid(row=4, column=0, sticky=W)

        # Password
        self.password = Label(self.content_frame, text="Password: ********", bg="#0F4C81", font=("Courier", 12))
        self.password.grid(row=5, column=0, sticky=W)

        # Predmeti
        self.subjects_text = Label(self.content_frame, text="Izabrani predmeti: ", bg="#0F4C81", font=("Courier", 12))
        self.subjects_text.grid(row=6, column=0, columnspan=2, sticky=W)
        self.subjects_listbox = Listbox(self.content_frame, selectmode="multiple", font=("Courier", 12), height=4,
                                        width=20)
        for sub in instructor.get_subjects():
            self.subjects_listbox.insert(END, sub)
        self.subjects_listbox.grid(row=8, column=0, columnspan=2)

        # Prosječna ocjena
        self.rating = Label(self.content_frame, text="Ocjena: " + str(instructor.get_average_rating()), bg="#0F4C81",
                            font=("Courier", 12))
        self.rating.grid(row=9, column=0, sticky=W, pady=(15, 0))

    # Prikaz views-a
    def run_homepage_instructor_view(self) -> None:
        self.homepage_win.mainloop()

    # Update-ovanje views-a
    def update_homepage_view(self, instructor) -> None:
        self.first_name.configure(text="Ime: " + instructor.get_first_name())
        self.last_name.configure(text="Prezime: " + instructor.get_last_name())
        self.email.configure(text="Email: " + instructor.get_email())
        self.username.configure(text="Username: " + instructor.get_username())
        self.password.configure(text="Password: ********")

        if instructor.get_image() is not None:
            with open("../images/tmp.jpg", "wb") as img:
                img.write(instructor.get_image())
            self.user_image = ImageTk.PhotoImage(Image.open("../images/tmp.jpg").resize((150, 150)))
        self.image = Label(self.content_frame, image=self.user_image)
        self.image.grid(row=0, column=0, columnspan=2, pady=(20, 20))

        self.subjects_listbox.delete(0, END)
        for sub in instructor.get_subjects():
            self.subjects_listbox.insert(END, sub)

        self.rating.configure(text="Ocjena: " + str(instructor.get_average_rating()))

    # Zatvaranje prozora
    def on_closing_homepage(self, view, root) -> None:
        ans = messagebox.askokcancel("Izlaz", "Da li ste sigurni da želite zatvoriti aplikaciju?")
        if ans:
            for win in view.get_top_level_wins():
                win.destroy()
            if str(root.state()) == "withdrawn":
                root.deiconify()
