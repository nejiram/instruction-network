from tkinter import *
from views.RegisterUserView import RegisterUserView


class RegisterStudentView(RegisterUserView):
    def __init__(self, root, subjects):
        RegisterUserView.__init__(self, root, subjects)
        self.subjects_label.configure(text="Izaberite predmet/predmete \niz kojih tražite instrukcije: ")
