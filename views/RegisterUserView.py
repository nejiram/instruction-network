from tkinter import *
from PIL import ImageTk, Image
from tkinter import filedialog
from tkinter import messagebox

FILETYPES = (("jpg", ".*jpg"), ("jpeg", "*.jpeg"), ("png", "*.png"))


class RegisterUserView:
    def __init__(self, root, subjects):
        # Registracija
        self.register_win = Toplevel(root)
        self.register_win.resizable(False, False)
        self.register_win.geometry("400x600")
        self.register_win.title("Instruction Network - Registracija")
        self.register_win.protocol("WM_DELETE_WINDOW", lambda: self.on_closing_register_view(root))
        self.register_win.iconbitmap("../images/instruction_network_icon.ico")
        self.register_win.configure(bg="#0F4C81")

        # Frame - podaci za regitraciju
        self.info_frame = Frame(self.register_win, bg="#0F4C81", padx=20, pady=20)
        self.info_frame.grid(row=0, column=0)
        self.info_frame.place(relx=.075, rely=.025)

        # Ime
        self.first_name_label = Label(self.info_frame, text="Ime: ", bg="#0F4C81", font=("Courier", 12))
        self.first_name_label.grid(row=0, column=0, sticky=W)
        self.first_name_entry = Entry(self.info_frame, width=30)
        self.first_name_entry.grid(row=0, column=1, sticky=E)

        # Prezime
        self.last_name_label = Label(self.info_frame, text="Prezime: ", bg="#0F4C81", font=("Courier", 12))
        self.last_name_label.grid(row=1, column=0, sticky=W)
        self.last_name_entry = Entry(self.info_frame, width=30)
        self.last_name_entry.grid(row=1, column=1, sticky=E)

        # Email
        self.email_label = Label(self.info_frame, text="Email: ", bg="#0F4C81", font=("Courier", 12))
        self.email_label.grid(row=2, column=0, sticky=W)
        self.email_entry = Entry(self.info_frame, width=30)
        self.email_entry.grid(row=2, column=1, sticky=E)

        # Username
        self.username_label = Label(self.info_frame, text="Username: ", bg="#0F4C81", font=("Courier", 12))
        self.username_label.grid(row=3, column=0, sticky=W)
        self.username_entry = Entry(self.info_frame, width=30)
        self.username_entry.grid(row=3, column=1, sticky=E)

        # Password
        self.password_label = Label(self.info_frame, text="Password: ", bg="#0F4C81", font=("Courier", 12))
        self.password_label.grid(row=4, column=0, sticky=W)
        self.password_entry = Entry(self.info_frame, show="*", width=30)
        self.password_entry.grid(row=4, column=1, sticky=E)

        # Slika korisnika
        self.has_image = False
        self.register_win.filename = None
        self.user_image = ImageTk.PhotoImage(Image.open("../images/user_icon.png").resize((150, 150)))
        self.user_image_label = Label(self.info_frame, image=self.user_image)
        self.user_image_label.grid(row=5, column=0, columnspan=2, pady=(20, 0))

        # Button - Učitaj sliku
        self.open_image_button = Button(self.info_frame, text="Učitaj sliku", font=("Courier", 8), bg="#658DC6",
                                        command=self.load_image)
        self.open_image_button.grid(row=6, column=0, columnspan=2, pady=(5, 10))

        # Predmeti
        self.subjects = subjects
        self.subjects_label = Label(self.info_frame, text="Izaberite predmet/predmete: ", bg="#0F4C81",
                                    font=("Courier", 12))
        self.subjects_label.grid(row=7, column=0, columnspan=2, sticky=W)
        self.subjects_listbox = Listbox(self.info_frame, selectmode="multiple", font=("Courier", 12), height=4,
                                        width=20)
        for sub in self.subjects:
            self.subjects_listbox.insert(END, sub)
        self.subjects_listbox.grid(row=8, column=0, columnspan=2, pady=(15, 15))

        # Button - regitriraj se
        self.register_button = Button(self.register_win, text="REGISTRIRAJ SE", font=("Courier", 12), width=20, bg="#658DC6")
        self.register_button.grid(row=1, column=0, columnspan=2, sticky=W + E)
        self.register_button.place(relx=.225, rely=.875)

    # Učitavanje slike
    def load_image(self) -> None:
        self.register_win.filename = filedialog.askopenfilename(initialdir="/", title="Odaberite sliku",
                                                                filetypes=FILETYPES)
        if len(self.register_win.filename) != 0:
            self.has_image = True
            self.user_image = ImageTk.PhotoImage(Image.open(self.register_win.filename).resize((150, 150)))
            self.user_image_label = Label(self.info_frame, image=self.user_image)
            self.user_image_label.grid(row=5, column=0, columnspan=2, pady=(20, 0))

    # Prikaz view-a
    def run_register_view(self) -> None:
        self.register_win.mainloop()

    # Zatvaranje prozora
    def on_closing_register_view(self, root) -> None:
        ans = messagebox.askyesnocancel("Izlaz", "Da li ste sigurni da želite prekinuti registraciju?")
        if ans:
            self.register_win.destroy()
            if str(root.state()) == "withdrawn":
                root.deiconify()

