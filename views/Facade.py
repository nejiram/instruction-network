from __future__ import annotations
from tkinter import *
from PIL import ImageTk, Image
from typing import Callable
from views.AccountView import AccountView
from views.CreateAppointmentView import CreateAppointmentView
from views.CreatedAppointmentsView import CreatedAppointmentsView
from views.EditAccountView import EditAccountView
from views.FilterInstructorsView import FilterInstructorsView
from views.HomepageInstructorView import HomepageInstructorView
from views.HomepageStudentView import HomepageStudentView
from views.InstructorReviewView import InstructorReviewView
from views.LoginView import LoginView
from views.RegisterInstructorView import RegisterInstructorView
from views.RegisterStudentView import RegisterStudentView
from views.ReservationsInstructorView import ReservationsInstructorView
from views.ReservationsStudentView import ReservationStudentView


class Facade:
    # konstruktor
    def __init__(self, login_view: LoginView):
        self.my_account_student_view = ""
        self.create_appointment_view = ""
        self.created_appointments_view = ""
        self.edit_account_view = ""
        self.filter_instructors_view = ""
        self.homepage_instructor_view = ""
        self.homepage_student_view = ""
        self.instructor_review_view = ""
        self.login_view = login_view
        self.register_instructor_view = ""
        self.register_student_view = ""
        self.reservations_instructor_view = ""
        self.reservations_student_view = ""

    # pridruzivanje metode button-u
    def attach_button_command(self, button: Button, command: Callable[[], None]) -> None:
        button.configure(command=command)

    # vraca AccountView
    def get_my_account_student_view(self, student) -> AccountView:
        self.my_account_student_view = AccountView(self.login_view.login_win, student)
        self.login_view.add_top_level_win(self.my_account_student_view.account_win)
        return self.my_account_student_view

    # update-ovanje AccountView-a
    def notify_student_account_view(self, student) -> None:
        self.my_account_student_view.update_account_view(student)

    # vraca CreateAppointmentView
    def get_create_appointment_view(self, subjects, locations) -> CreateAppointmentView:
        self.create_appointment_view = CreateAppointmentView(self.login_view.login_win, subjects, locations)
        self.login_view.add_top_level_win(self.create_appointment_view.create_appointment_win)
        return self.create_appointment_view

    # vraca CreatedAppointmentView
    def get_created_appointments_view(self, past_appointments, future_appointments) -> CreatedAppointmentsView:
        self.created_appointments_view = CreatedAppointmentsView(self.login_view.login_win, past_appointments,
                                                                 future_appointments)
        self.login_view.add_top_level_win(self.created_appointments_view.created_appointments_win)
        return self.created_appointments_view

    # update-ovanje CreatedAppointmentView-a
    def notify_created_appointments_view(self, future_appointments) -> None:
        self.created_appointments_view.update_created_appointments_view(future_appointments)

    # vraca EditAccountView
    def get_edit_account_view(self, user, subjects) -> EditAccountView:
        self.edit_account_view = EditAccountView(self.login_view.login_win, subjects, user.get_subjects())
        self.edit_account_view.first_name_entry.insert(0, user.get_first_name())
        self.edit_account_view.last_name_entry.insert(0, user.get_last_name())
        self.edit_account_view.email_entry.insert(0, user.get_email())
        self.edit_account_view.username_entry.insert(0, user.get_username())
        self.edit_account_view.password_entry.insert(0, user.get_password())
        if user.get_image() is not None:
            with open("../images/tmp.jpg", "wb") as img:
                img.write(user.get_image())
            self.edit_account_view.user_image = ImageTk.PhotoImage(Image.open("../images/tmp.jpg").resize((150, 150)))
            self.edit_account_view.user_image_label.configure(image=self.edit_account_view.user_image)
        self.login_view.add_top_level_win(self.edit_account_view.edit_acc_win)
        return self.edit_account_view

    # zatvaranje Homepa-e i prikaz Login-a nakon brisanja - student
    def delete_students_account(self):
        self.homepage_student_view.homepage_win.destroy()
        self.login_view.login_win.deiconify()

    # zatvaranje Homepa-e i prikaz Login-a nakon brisanja - instruktor
    def delete_instructors_account(self):
        self.homepage_instructor_view.homepage_win.destroy()
        self.login_view.login_win.deiconify()

    # vraca FilterInstructorsView
    def get_filter_instructors_view(self, subjects) -> FilterInstructorsView:
        self.filter_instructors_view = FilterInstructorsView(self.login_view.login_win, subjects)
        self.login_view.add_top_level_win(self.filter_instructors_view.filter_instructors_win)
        return self.filter_instructors_view

    # vraca HomepageInstructorView
    def get_homepage_instructor_view(self, instructor) -> HomepageInstructorView:
        if str(self.login_view.login_win.state()) != "withdrawn":
            self.login_view.login_win.withdraw()
        self.homepage_instructor_view = HomepageInstructorView(self.login_view, self.login_view.login_win, instructor)
        self.login_view.add_top_level_win(self.homepage_instructor_view.homepage_win)
        return self.homepage_instructor_view

    # update-uje HomepageInstructorView
    def notify_homepage_instructor_view(self, instructor) -> None:
        self.homepage_instructor_view.update_homepage_view(instructor)

    # vraca HomepageStudentView
    def get_homepage_student_view(self, appointment) -> HomepageStudentView:
        if str(self.login_view.login_win.state()) != "withdrawn":
            self.login_view.login_win.withdraw()
        self.homepage_student_view = HomepageStudentView(self.login_view, self.login_view.login_win, appointment)
        self.login_view.add_top_level_win(self.homepage_student_view.homepage_win)
        return self.homepage_student_view

    # update-uje HomepageStudentView
    def notify_homepage_student_view(self, appointments) -> None:
        self.homepage_student_view.update_homepage_student_view(appointments)

    # vraca InstructorReviewView
    def get_instructor_review_view(self, instructor, grades, appointments) -> InstructorReviewView:
        self.instructor_review_view = InstructorReviewView(self.login_view.login_win, instructor, grades, appointments)
        self.login_view.add_top_level_win(self.instructor_review_view.account_win)
        return self.instructor_review_view

    # update-uje InstructorReviewView sa terminima
    def notify_appointments_instructor_review_view(self, appointments: []) -> None:
        self.instructor_review_view.update_appointments(appointments)

    # update-uje InstructorReviewView sa ocjenama
    def notify_rating_instructor_review_view(self, rating: float) -> None:
        self.instructor_review_view.update_rating(str(rating))

    # vraca LoginView
    def get_login_view(self) -> LoginView:
        return self.login_view

    # vraca RegisterInstructorView
    def get_register_instructor_view(self, subjects) -> RegisterInstructorView:
        self.login_view.login_win.withdraw()
        self.register_instructor_view = RegisterInstructorView(self.login_view.login_win, subjects)
        self.login_view.add_top_level_win(self.register_instructor_view.register_win)
        return self.register_instructor_view

    # vraca RegisterStudentView
    def get_register_student_view(self, subjects) -> RegisterStudentView:
        self.login_view.login_win.withdraw()
        self.register_student_view = RegisterStudentView(self.login_view.login_win, subjects)
        self.login_view.add_top_level_win(self.register_student_view.register_win)
        return self.register_student_view

    # vraca ReservationsInstructorView
    def get_reservations_instructor_view(self, reservations) -> ReservationsInstructorView:
        self.reservations_instructor_view = ReservationsInstructorView(self.login_view.login_win, reservations)
        self.login_view.add_top_level_win(self.reservations_instructor_view.reservations_win)
        return self.reservations_instructor_view

    # update-uje ReservationsInstructorsView
    def notify_reservations_instructor_view(self, reservations):
        self.reservations_instructor_view.update_reservations_instructor_view(reservations)

    # vraca ReservationsStudentView
    def get_reservations_student_view(self, resevations) -> ReservationStudentView:
        self.reservations_student_view = ReservationStudentView(self.login_view.login_win, resevations)
        self.login_view.add_top_level_win(self.reservations_student_view.reservations_win)
        return self.reservations_student_view

    # update-uje ReservationsStudentView
    def notify_reservations_student_view(self, reservations) -> None:
        self.reservations_student_view.update_reservations_student_view(reservations)
