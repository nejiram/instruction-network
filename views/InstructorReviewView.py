from tkinter import *
from views.AccountView import AccountView
from tkinter.ttk import Combobox


class InstructorReviewView(AccountView):
    # Konstruktor
    def __init__(self, root, instructor, grades, appointments):
        AccountView.__init__(self, root, instructor)
        # Pregled informacija o instruktoru
        self.account_win.geometry("850x550")
        self.account_win.title("Instruction Network - Instruktor")

        self.password.destroy()
        self.edit_button.destroy()
        self.delete_button.destroy()

        # Ocjene
        self.rating = Label(self.info_frame, text="Ocjena: " + str(instructor.get_average_rating()), bg="#0F4C81",
                            font=("Courier", 12))
        self.rating.grid(row=9, column=0, sticky=W, pady=(15, 0))
        self.info_frame.place(relx=.02)

        # Frame - termini
        self.appointments_frame = Frame(self.account_win, bg="#0F4C81", padx=20, pady=20)
        self.appointments_frame.grid(row=0, column=1)
        self.appointments_frame.place(relx=.35, rely=.025)

        # Label - Dostupni Termini
        self.appointment_label = Label(self.appointments_frame, text="DOSTUPNI TERMINI: ", bg="#0F4C81",
                                       font=("Courier", 12, "bold"))
        self.appointment_label.grid(row=0, column=0, columnspan=2, sticky=W + E)
        self.help_label = Label(self.appointments_frame, text="Klknite na termin kako biste ga rezervirali.",
                                bg="#0F4C81", font=("Courier", 9, "italic"))
        self.help_label.grid(row=1, column=0, columnspan=2, sticky=W + E)

        # Scrollbar - Dostupni termini
        self.scrollbar_frame = Frame(self.appointments_frame, bg="#0F4C81", padx=20, pady=20)
        self.scrollbar_frame.grid(row=2, column=0)

        self.scrollbar_appointments = Scrollbar(self.scrollbar_frame)
        self.scrollbar_appointments.pack(side=RIGHT, fill=Y)

        self.appointments_listbox = Listbox(self.scrollbar_frame, selectmode="single", font=("Courier", 12),
                                            width=45, height=8,
                                            yscrollcommand=self.scrollbar_appointments.set)
        # Dostupni termini
        for a in appointments:
            online = ""
            location = ", " + a.get_location()
            if a.get_is_online():
                online = ", online"
                location = ""
            tmp = a.get_subject() + ", " + a.get_date() + ", " + a.get_start_time() + "-" + a.get_end_time() + online + location + ", " + str(
                a.get_price())
            self.appointments_listbox.insert(END, tmp)
        self.scrollbar_appointments.config(command=self.appointments_listbox.yview)
        self.appointments_listbox.pack()

        # Button - Rezerviraj
        self.reserve_button = Button(self.appointments_frame, text="REZERVIRAJ", font=("Courier", 12), width=45,
                                     bg="#658DC6")
        self.reserve_button.grid(row=3, column=0, columnspan=2)

        # Ocjenjivanje intruktora
        self.rate_label = Label(self.appointments_frame, text="Izaberi ocjenu: ", bg="#0F4C81",
                                font=("Courier", 12))
        self.rate_label.grid(row=4, column=0, columnspan=2, sticky=W + E, pady=(20, 0))
        self.grade_value = IntVar()
        self.grades_combobox = Combobox(self.appointments_frame, font=("Courier", 12), height=5,
                                        width=20, values=grades, state="readonly",
                                        textvariable=self.grade_value)
        self.grades_combobox.current(0)
        self.grades_combobox.grid(row=5, column=0, columnspan=2, pady=(0, 15))

        # Button - Ocijeni
        self.rate_button = Button(self.appointments_frame, text="OCIJENI", font=("Courier", 12), width=20,
                                  bg="#658DC6")
        self.rate_button.grid(row=6, column=0, columnspan=2)

    # Prikaz views-a
    def run_instructor_review_view(self) -> None:
        self.account_win.mainloop()

    # Update-ovanje termina
    def update_appointments(self, appointments) -> None:
        self.appointments_listbox.delete(0, END)
        for a in appointments:
            online = ""
            location = ", " + a.get_location()
            if a.get_is_online():
                online = ", online"
                location = ""
            tmp = a.get_subject() + ", " + a.get_date() + ", " + a.get_start_time() + "-" + a.get_end_time() + online + location + ", " + str(
                a.get_price())
            self.appointments_listbox.insert(END, tmp)
        self.appointments_listbox.pack()

    # Update-ovanje ocjene
    def update_rating(self, rating: str) -> None:
        self.rating.configure(text="Ocjena: " + rating)
