from tkinter import *
from tkinter.ttk import Combobox


class CreateAppointmentView:
    # Konstruktor
    def __init__(self, root, subjects, locations):
        # Kreiranje termina
        self.create_appointment_win = Toplevel(root)
        self.create_appointment_win.resizable(False, False)
        self.create_appointment_win.geometry("400x450")
        self.create_appointment_win.title("Instruction Network - Kreiranje Termina")
        self.create_appointment_win.iconbitmap("../images/instruction_network_icon.ico")
        self.create_appointment_win.configure(bg="#0F4C81")

        # Frame - informacije o terminu
        self.appointment_frame = Frame(self.create_appointment_win, bg="#0F4C81", padx=20, pady=20)
        self.appointment_frame.grid(row=0, column=0, pady=(15, 15))

        # Predmet
        self.subjects = subjects
        self.subject_name_label = Label(self.appointment_frame, text="Predmet: ", bg="#0F4C81", font=("Courier", 12))
        self.subject_name_label.grid(row=0, column=0, sticky=W)
        self.subjects_listbox = Listbox(self.appointment_frame, selectmode="single", font=("Courier", 12), height=4,
                                        width=20)
        for sub in self.subjects:
            self.subjects_listbox.insert(END, sub)
        self.subjects_listbox.grid(row=1, column=0, columnspan=2, pady=(15, 15))

        # Datum termina
        self.date_label = Label(self.appointment_frame, text="Datum: ", bg="#0F4C81",
                                font=("Courier", 12))
        self.date_label.grid(row=2, column=0, sticky=W)
        self.date_entry = Entry(self.appointment_frame, width=30)
        self.date_entry.grid(row=2, column=1, sticky=E)

        # Vrijeme početka termina
        self.start_time_label = Label(self.appointment_frame, text="Vrijeme početka: ", bg="#0F4C81",
                                      font=("Courier", 12))
        self.start_time_label.grid(row=3, column=0, sticky=W)
        self.start_time_entry = Entry(self.appointment_frame, width=30)
        self.start_time_entry.grid(row=3, column=1, sticky=E)

        # Vrijeme kraja termina
        self.end_time_label = Label(self.appointment_frame, text="Vrijeme kraja: ", bg="#0F4C81",
                                    font=("Courier", 12))
        self.end_time_label.grid(row=4, column=0, sticky=W)
        self.end_time_entry = Entry(self.appointment_frame, width=30)
        self.end_time_entry.grid(row=4, column=1, sticky=E)

        # Online
        self.online_var = BooleanVar()
        self.online_var.set(False)
        self.online_checkbox = Checkbutton(self.appointment_frame, text="Online termin", variable=self.online_var,
                                           onvalue="True",
                                           offvalue="False", bg="#0F4C81", font=("Courier", 12))
        self.online_checkbox.grid(row=5, column=0, columnspan=2, sticky=W + E)

        # Lokacija
        self.location_value = StringVar()
        self.location_label = Label(self.appointment_frame, text="Lokacija: ", bg="#0F4C81", font=("Courier", 12))
        self.location_label.grid(row=6, column=0, sticky=W)
        self.locations_combobox = Combobox(self.appointment_frame, font=("Courier", 12), height=5,
                                           width=20, values=locations, state="readonly",
                                           textvariable=self.location_value)
        self.locations_combobox.current(0)
        self.locations_combobox.grid(row=7, column=0, columnspan=2, pady=(15, 15))

        # Cijena
        self.price_label = Label(self.appointment_frame, text="Cijena (KM): ", bg="#0F4C81", font=("Courier", 12))
        self.price_label.grid(row=8, column=0, sticky=W)
        self.price_entry = Entry(self.appointment_frame, width=30)
        self.price_entry.grid(row=8, column=1, sticky=E)

        # Button - Kreiraj termin
        self.create_button = Button(self.create_appointment_win, text="KREIRAJ", font=("Courier", 12), width=20,
                                    bg="#658DC6")
        self.create_button.grid(row=1, column=0, columnspan=2, sticky=W + E)
        self.create_button.place(relx=.225, rely=.875)

    # Prikaz views-a
    def run_created_appointment_view(self) -> None:
        self.create_appointment_win.mainloop()
