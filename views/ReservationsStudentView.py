from tkinter import *
from views.ReservationsInstructorView import ReservationsInstructorView


class ReservationStudentView(ReservationsInstructorView):
    # Konstruktor
    def __init__(self, root, reservations):
        ReservationsInstructorView.__init__(self, root, reservations)
        self.reservations_win.geometry("650x400")

        if len(reservations) != 0:
            self.reservations_listbox.destroy()

            self.reservations_listbox = Listbox(self.reservations_frame,
                                                yscrollcommand=self.scrollbar_reservations.set, bg="#0F4C81",
                                                font=("Courier", 10), width=73, selectmode="single", height=15)
            self.scrollbar_reservations.config(command=self.reservations_listbox.yview)

            for r in reservations:
                online = ""
                location = r.get_appointment().get_location()
                if r.get_appointment().get_is_online():
                    online = "online"
                    location = ""
                tmp = str(reservations.index(r) + 1) + ". " + r.get_appointment().get_instructor().get_first_name() +\
                      " " + r.get_appointment().get_instructor().get_last_name() + ", " +  r.get_appointment().get_subject() +\
                      ", " + r.get_appointment().get_date() + ", " + r.get_appointment().get_start_time() + "-" + \
                      r.get_appointment().get_end_time() + ", " + online + location
                self.reservations_listbox.insert(END, tmp)
                self.reservations_listbox.pack()

    # Prikaz views-a
    def run_reservations_student_view(self) -> None:
        self.reservations_win.mainloop()

    # Update-ovanje views-a
    def update_reservations_student_view(self, reservations) -> None:
        self.reservations_listbox.delete(0, END)
        if len(reservations) != 0:
            self.reservations_listbox.destroy()

            self.reservations_listbox = Listbox(self.reservations_frame,
                                                yscrollcommand=self.scrollbar_reservations.set, bg="#0F4C81",
                                                font=("Courier", 10), width=60, selectmode="single", height=15)
            self.scrollbar_reservations.config(command=self.reservations_listbox.yview)

            for r in reservations:
                online = ""
                location = r.get_appointment().get_location()
                if r.get_appointment().get_is_online():
                    online = "online"
                    location = ""
                tmp = str(reservations.index(r) + 1) + ". " + r.get_appointment().get_instructor().get_first_name() + \
                      " " + r.get_appointment().get_instructor().get_last_name() + ", " + r.get_appointment().get_subject() +\
                      ", " + r.get_appointment().get_date() + ", " + r.get_appointment().get_start_time() + "-" +\
                      r.get_appointment().get_end_time() + ", " + online + location
                self.reservations_listbox.insert(END, tmp)
                self.reservations_listbox.pack()
