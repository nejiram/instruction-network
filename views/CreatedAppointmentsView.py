from tkinter import *


class CreatedAppointmentsView:
    # Konstruktor
    def __init__(self, root, past_appointments, future_appointments):
        # Kreirani termini
        self.past_appointments = past_appointments
        self.future_appointments = future_appointments

        self.created_appointments_win = Toplevel(root)
        self.created_appointments_win.resizable(False, False)
        self.created_appointments_win.geometry("1020x550")
        self.created_appointments_win.title("Instruction Network - Kreirani Termini")
        self.created_appointments_win.iconbitmap("../images/instruction_network_icon.ico")
        self.created_appointments_win.configure(bg="#0F4C81")

        # Future - Predstojeći termini
        self.future_appointments_frame = Frame(self.created_appointments_win, bg="#0F4C81", padx=20, pady=20)
        self.future_appointments_frame.grid(row=1, column=0)

        # Label - Predstojeći termini
        self.future_appointments_label = Label(self.future_appointments_frame, text="Predstojeći termini: ",
                                               bg="#0F4C81",
                                               font=("Courier", 14, "bold"))
        self.future_appointments_label.pack()
        self.text_label = Label(self.future_appointments_frame, text="Kliknite na termin da biste ga obrisali!",
                                bg="#0F4C81", font=("Courier", 10, "bold"))
        self.text_label.pack()

        # Scrollbar - predstojeći termini
        self.scrollbar_future_appointments = Scrollbar(self.future_appointments_frame)
        self.scrollbar_future_appointments.pack(side=RIGHT, fill=Y)
        self.future_appointments_listbox = Listbox(self.future_appointments_frame,
                                                   yscrollcommand=self.scrollbar_future_appointments.set, bg="#0F4C81",
                                                   font=("Courier", 10), width=120, selectmode="single", height=10)
        self.scrollbar_future_appointments.config(command=self.future_appointments_listbox.yview)

        # Predstojeći termini
        for a in self.future_appointments:
            online = "ne"
            if a.get_is_online():
                online = "da"
            tmp = str(future_appointments.index(a) + 1) + ". PREDMET: " + a.get_subject() + ", DATUM: " + a.get_date() + \
                  ", POČETAK: " + a.get_start_time() + ", KRAJ: " + a.get_end_time() + ", ONLINE: " + online + \
                  ", LOKACIJA: " + a.get_location() + ", CIJENA: " + str(a.get_price())
            self.future_appointments_listbox.insert(END, tmp)
            self.future_appointments_listbox.pack()

        # Button - Obriši termin
        self.delete_appointment_button = Button(self.created_appointments_win, text="OBRIŠI TERMIN",
                                                font=("Courier", 12),
                                                width=20, bg="#658DC6")
        self.delete_appointment_button.grid(row=2, column=0, columnspan=2)

        # Frame - Prošli termini
        self.past_appointments_frame = Frame(self.created_appointments_win, bg="#0F4C81", padx=20, pady=20)
        self.past_appointments_frame.grid(row=3, column=0)

        # Label - Prošli termini
        self.past_appointments_label = Label(self.past_appointments_frame, text="Prošli termini: ", bg="#0F4C81",
                                             font=("Courier", 14, "bold"))
        self.past_appointments_label.pack()

        if len(self.past_appointments) != 0:
            # Scrollbar - Prošli termini
            self.scrollbar_past_appointments = Scrollbar(self.past_appointments_frame)
            self.scrollbar_past_appointments.pack(side=RIGHT, fill=Y)
            self.past_appointments_text = Text(self.past_appointments_frame, wrap=WORD,
                                               yscrollcommand=self.scrollbar_past_appointments.set,
                                               bg="#0F4C81", font=("Courier", 10), height=10)
            self.scrollbar_past_appointments.config(command=self.past_appointments_text.yview)

            # Prošli termini
            for a in self.past_appointments:
                online = "ne"
                if a.get_is_online():
                    online = "da"
                tmp = str(
                    past_appointments.index(a) + 1) + ". PREDMET: " + a.get_subject() + ", DATUM: " + a.get_date() + \
                      ",POČETAK: " + a.get_start_time() + ", KRAJ: " + a.get_end_time() + ", ONLINE: " + online + \
                      ", LOKACIJA: " + a.get_location() + ", CIJENA: " + str(a.get_price()) + "\n"
                self.past_appointments_text.insert(END, tmp)
                self.past_appointments_text.bind("<Key>", lambda a: "break")
                self.past_appointments_text.pack()
        else:
            # Nema termina koji su prošli
            self.else_label = Label(self.past_appointments_frame, text="Nema prošlih termina.", bg="#0F4C81",
                                    font=("Courier", 10, "bold"))
            self.else_label.pack()

    # Prikaz views-a
    def run_created_appointments_view(self) -> None:
        self.created_appointments_win.mainloop()

    # Update-ovanje views-a
    def update_created_appointments_view(self, future_appointments) -> None:
        self.future_appointments_listbox.delete(0, END)
        self.future_appointments = future_appointments
        for a in future_appointments:
            online = "ne"
            if a.get_is_online():
                online = "da"
            tmp = str(future_appointments.index(a) + 1) + ". PREDMET: " + a.get_subject() + ", DATUM: " + a.get_date() + \
                  ", POČETAK: " + a.get_start_time() + ", KRAJ: " + a.get_end_time() + ", ONLINE: " + online + \
                  ", LOKACIJA: " + a.get_location() + ", CIJENA: " + str(a.get_price())
            self.future_appointments_listbox.insert(END, tmp)
            self.future_appointments_listbox.pack()
