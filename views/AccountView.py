from tkinter import *
from PIL import ImageTk, Image


class AccountView:
    # Konstruktor
    def __init__(self, root, user):
        # Moj profil
        self.account_win = Toplevel(root)
        self.account_win.resizable(False, False)
        self.account_win.geometry("350x600")
        self.account_win.title("Instruction Network - Moj Profil")
        self.account_win.iconbitmap("../images/instruction_network_icon.ico")
        self.account_win.configure(bg="#0F4C81")

        # Frame - informacije o korisniku
        self.info_frame = Frame(self.account_win, bg="#0F4C81", padx=20, pady=20)
        self.info_frame.grid(row=0, column=0)
        self.info_frame.place(relx=.075, rely=.025)

        # Slika korisnika
        self.user_image = ImageTk.PhotoImage(Image.open("../images/user_icon.png").resize((150, 150)))
        if user.get_image() is not None:
            with open("../images/tmp.jpg", "wb") as img:
                img.write(user.get_image())
            self.user_image = ImageTk.PhotoImage(Image.open("../images/tmp.jpg").resize((150, 150)))
        self.image = Label(self.info_frame, image=self.user_image)
        self.image.grid(row=0, column=0, columnspan=2, pady=(20, 20))

        # Ime
        self.first_name = Label(self.info_frame, text="Ime: " + user.get_first_name(), bg="#0F4C81",
                                font=("Courier", 12))
        self.first_name.grid(row=1, column=0, sticky=W)

        # Prezime
        self.last_name = Label(self.info_frame, text="Prezime: " + user.get_last_name(), bg="#0F4C81",
                               font=("Courier", 12))
        self.last_name.grid(row=2, column=0, sticky=W)

        # Email
        self.email = Label(self.info_frame, text="Email: " + user.get_email(), bg="#0F4C81", font=("Courier", 12))
        self.email.grid(row=3, column=0, sticky=W)

        # Username
        self.username = Label(self.info_frame, text="Username: " + user.get_username(), bg="#0F4C81",
                              font=("Courier", 12))
        self.username.grid(row=4, column=0, sticky=W)

        # Password
        self.password = Label(self.info_frame, text="Password: ********", bg="#0F4C81", font=("Courier", 12))
        self.password.grid(row=5, column=0, sticky=W)

        # Izabrani predmeti
        self.subjects_text = Label(self.info_frame, text="Izabrani predmeti: ", bg="#0F4C81", font=("Courier", 12))
        self.subjects_text.grid(row=6, column=0, columnspan=2, sticky=W)
        self.subjects_listbox = Listbox(self.info_frame, selectmode="multiple", font=("Courier", 12), height=4,
                                        width=20)
        for sub in user.get_subjects():
            self.subjects_listbox.insert(END, sub)
        self.subjects_listbox.grid(row=8, column=0, columnspan=2)

        # Button - Uredi profil
        self.edit_button = Button(self.info_frame, text="UREDI PROFIL", font=("Courier", 12), width=20, bg="#658DC6")
        self.edit_button.grid(row=10, column=0, columnspan=2, sticky=W + E, pady=(15, 0))

        # Button - obriši profil
        self.delete_button = Button(self.info_frame, text="OBRIŠI RAČUN", font=("Courier", 12), width=20, bg="#658DC6")
        self.delete_button.grid(row=11, column=0, columnspan=2, sticky=W + E)

    # Prikaz views-a
    def run_my_account_view(self) -> None:
        self.account_win.mainloop()

    # Update-ovanje views-a
    def update_account_view(self, user) -> None:
        self.first_name.configure(text="Ime: " + user.get_first_name())
        self.last_name.configure(text="Prezime: " + user.get_last_name())
        self.email.configure(text="Email: " + user.get_email())
        self.username.configure(text="Username: " + user.get_username())
        self.password.configure(text="Password: ********")

        if user.get_image() is not None:
            with open("../images/tmp.jpg", "wb") as img:
                img.write(user.get_image())
            self.user_image = ImageTk.PhotoImage(Image.open("../images/tmp.jpg").resize((150, 150)))
        self.image = Label(self.info_frame, image=self.user_image)
        self.image.grid(row=0, column=0, columnspan=2, pady=(20, 20))

        self.subjects_listbox.delete(0, END)
        for sub in user.get_subjects():
            self.subjects_listbox.insert(END, sub)
