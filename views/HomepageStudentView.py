from tkinter import *
from tkinter import messagebox


class HomepageStudentView:
    # Konstruktor
    def __init__(self, view, root, appointment):
        self.appointments = appointment
        # Homepage Window
        self.homepage_win = Toplevel(root)
        self.homepage_win.protocol("WM_DELETE_WINDOW", lambda: self.on_closing_homepage(view, root))
        self.homepage_win.resizable(False, False)
        self.homepage_win.geometry("725x450")
        self.homepage_win.title("Instruction Network - Početna Stranica")
        self.homepage_win.iconbitmap("../images/instruction_network_icon.ico")
        self.homepage_win.configure(bg="#0F4C81")

        # Buttons Frame
        self.buttons_frame = Frame(self.homepage_win)
        self.buttons_frame.grid(row=0, column=0, pady=50)

        # Filter Button
        self.filter_button = Button(self.buttons_frame, text="FILTERI", font=("Courier", 12), width=15,
                                    bg="#658DC6", borderwidth=.5)
        self.filter_button.grid(row=0, column=0)

        # Account Button
        self.account_button = Button(self.buttons_frame, text="MOJ PROFIL", font=("Courier", 12), width=15,
                                     bg="#658DC6", borderwidth=.5)
        self.account_button.grid(row=1, column=0)

        # Reservations Button
        self.reservations_button = Button(self.buttons_frame, text="PREGLED\nREZERVACIJA", font=("Courier", 12),
                                          width=15, bg="#658DC6", borderwidth=.5)
        self.reservations_button.grid(row=2, column=0)

        # Logout Button
        self.logout_button = Button(self.buttons_frame, text="LOG OUT", font=("Courier", 12), width=15,
                                    bg="#658DC6", borderwidth=.5, command=lambda: self.on_closing_homepage(view, root))
        self.logout_button.grid(row=3, column=0)

        # Content Frame
        self.content_frame = Frame(self.homepage_win, bg="#0F4C81")
        self.content_frame.grid(row=0, column=1, padx=(5, 5))

        # Content
        self.heading_label = Label(self.content_frame, text="DOSTUPNI TERMINI: ", bg="#0F4C81",
                                   font=("Courier", 14, "bold"))
        self.heading_label.pack()
        self.help_label = Label(self.content_frame, text="Klikom na termin otvarate profil instruktora.",
                                bg="#0F4C81",
                                font=("Courier", 12))
        self.help_label.pack()
        self.scrollbar = Scrollbar(self.content_frame)
        self.scrollbar.pack(side=RIGHT, fill=Y)

        self.instructors_listbox = Listbox(self.content_frame, selectmode="single", width=65, height=18, bg="#0F4C81",
                                           font=("Courier", 10), borderwidth=.5)
        for a in self.appointments:
            username = "@" + a.get_instructor().get_username()
            subject = a.get_subject()
            date = a.get_date()
            time = a.get_start_time() + "-" + a.get_end_time()
            online = ""
            location = a.get_location()
            if a.get_is_online():
                online = "online"
                location = ""
            price = str(a.get_price())
            tmp = username + ", " + subject + ", " + date + ", " + time + ", " + online + location + ", " + price
            self.instructors_listbox.insert(END, tmp)
        self.instructors_listbox.configure(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.instructors_listbox.yview)
        self.instructors_listbox.pack()

        # Button - Prikaz instruktora
        self.show_instructor_button = Button(self.homepage_win, text="PRIKAŽI INSTRUKTORA", font=("Courier", 12), width=30,
                                    bg="#658DC6", borderwidth=.5)
        self.show_instructor_button.grid(row=1, column=1, pady=(10,10))

    # Prikaz views-a
    def run_homepage_student_view(self) -> None:
        self.homepage_win.mainloop()

    # Update-ovanje views-a
    def update_homepage_student_view(self, appointments) -> None:
        self.instructors_listbox.delete(0, END)
        self.appointments = appointments
        for a in self.appointments:
            username = "@" + a.get_instructor().get_username()
            subject = a.get_subject()
            date = a.get_date()
            time = a.get_start_time() + "-" + a.get_end_time()
            online = ""
            location = a.get_location()
            if a.get_is_online():
                online = "online"
                location = ""
            price = str(a.get_price())
            tmp = username + ", " + subject + ", " + date + ", " + time + ", " + online + location + ", " + price
            self.instructors_listbox.insert(END, tmp)
        self.instructors_listbox.configure(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.instructors_listbox.yview)
        self.instructors_listbox.pack()

    # Zatvaranje prozora
    def on_closing_homepage(self, view, root) -> None:
        ans = messagebox.askokcancel("Izlaz", "Da li ste sigurni da želite zatvoriti aplikaciju?")
        if ans:
            for win in view.get_top_level_wins():
                win.destroy()
            if str(root.state()) == "withdrawn":
                root.deiconify()