from tkinter import *
from tkinter.ttk import Combobox

LOCATIONS = ("--", "Sarajevo", "Tuzla", "Zenica", "Mostar", "Banja Luka")


class FilterInstructorsView:
    # Konstruktor
    def __init__(self, root, subjects):
        self.subjects = subjects

        # Filtriranje termina
        self.filter_instructors_win = Toplevel(root)
        self.filter_instructors_win.resizable(False, False)
        self.filter_instructors_win.geometry("300x650")
        self.filter_instructors_win.title("Instruction Network - Filteri")
        self.filter_instructors_win.iconbitmap("../images/instruction_network_icon.ico")
        self.filter_instructors_win.configure(bg="#B5C7D3")

        # Frame - filteri
        self.filters_frame = Frame(self.filter_instructors_win, bg="#B5C7D3")
        self.filters_frame.grid(row=0, column=0, padx=(20, 20), pady=(20, 20))
        self.filters_frame.place(relx=.075, rely=.05)

        # Filtriranje po predmetu
        self.subject_name_label = Label(self.filters_frame, text="Predmeti: ", bg="#B5C7D3", font=("Courier", 12))
        self.subject_name_label.grid(row=0, column=0, sticky=W, pady=(10, 0))
        self.subjects_listbox = Listbox(self.filters_frame, selectmode="multiple", font=("Courier", 12), height=4,
                                        width=20)
        for sub in self.subjects:
            self.subjects_listbox.insert(END, sub)
        self.subjects_listbox.grid(row=1, column=0, columnspan=2, pady=(15, 15))

        # Filtriranje po datumu
        self.date_frame = LabelFrame(self.filters_frame, text="DATUM", bg="#B5C7D3")
        self.date_frame.grid(row=2, column=0, columnspan=2, pady=(0, 15))

        self.from_date_label = Label(self.date_frame, text="Od: ", bg="#B5C7D3", font=("Courier", 12))
        self.from_date_label.grid(row=0, column=0, sticky=W)
        self.from_date_entry = Entry(self.date_frame, width=30)
        self.from_date_entry.grid(row=0, column=1, sticky=E, padx=(0, 5))

        self.to_date_label = Label(self.date_frame, text="Do: ", bg="#B5C7D3", font=("Courier", 12))
        self.to_date_label.grid(row=1, column=0, sticky=W)
        self.to_date_entry = Entry(self.date_frame, width=30)
        self.to_date_entry.grid(row=1, column=1, sticky=E, padx=(0, 5))

        # Filtriranje po vremenu
        self.time_frame = LabelFrame(self.filters_frame, text="VRIJEME", bg="#B5C7D3")
        self.time_frame.grid(row=3, column=0, columnspan=2, pady=(0, 15))

        self.from_time_label = Label(self.time_frame, text="Od: ", bg="#B5C7D3", font=("Courier", 12))
        self.from_time_label.grid(row=0, column=0, sticky=W)
        self.from_time_entry = Entry(self.time_frame, width=30)
        self.from_time_entry.grid(row=0, column=1, sticky=E, padx=(0, 5))

        self.to_time_label = Label(self.time_frame, text="Do: ", bg="#B5C7D3", font=("Courier", 12))
        self.to_time_label.grid(row=1, column=0, sticky=W)
        self.to_time_entry = Entry(self.time_frame, width=30)
        self.to_time_entry.grid(row=1, column=1, sticky=E, padx=(0, 5))

        # Filtriranje - da li je termin online
        self.online_var = BooleanVar()
        self.online_var.set(False)
        self.online_checkbox = Checkbutton(self.filters_frame, text="Online termin: ", variable=self.online_var,
                                           onvalue="True", offvalue="False", bg="#B5C7D3", font=("Courier", 12))
        self.online_checkbox.grid(row=4, column=0, columnspan=2, sticky=W + E, pady=(0, 15))

        # Filtriranje po lokaciji
        self.location_value = StringVar()
        self.location_label = Label(self.filters_frame, text="Lokacija: ", bg="#B5C7D3", font=("Courier", 12))
        self.location_label.grid(row=5, column=0, sticky=W)
        self.locations_combobox = Combobox(self.filters_frame, font=("Courier", 12), height=5,
                                           width=20, values=LOCATIONS, state="readonly",
                                           textvariable=self.location_value)
        self.locations_combobox.current(0)
        self.locations_combobox.grid(row=6, column=0, columnspan=2, pady=(0, 15))

        # Filtriranje po cijeni
        self.price_frame = LabelFrame(self.filters_frame, text="CIJENA", bg="#B5C7D3")
        self.price_frame.grid(row=7, column=0, columnspan=2, pady=(0, 15))

        self.from_price_label = Label(self.price_frame, text="Od: ", bg="#B5C7D3", font=("Courier", 12))
        self.from_price_label.grid(row=0, column=0, sticky=W)
        self.from_price_entry = Entry(self.price_frame, width=30)
        self.from_price_entry.grid(row=0, column=1, sticky=E, padx=(0, 5))

        self.to_price_label = Label(self.price_frame, text="Do: ", bg="#B5C7D3", font=("Courier", 12))
        self.to_price_label.grid(row=1, column=0, sticky=W)
        self.to_price_entry = Entry(self.price_frame, width=30)
        self.to_price_entry.grid(row=1, column=1, sticky=E, padx=(0, 5))

        # Button - poništavanje filtera
        self.reset_button = Button(self.filters_frame, text="PONIŠTI FILTERE", font=("Courier", 12), width=20,
                                   bg="#658DC6")
        self.reset_button.grid(row=8, column=0, columnspan=2, sticky=W + E)

        # Button - filtriranje
        self.filter_button = Button(self.filters_frame, text="FILTRIRAJ", font=("Courier", 12), width=20, bg="#658DC6")
        self.filter_button.grid(row=9, column=0, columnspan=2, sticky=W + E, pady=(0, 5))

    # Prikaz views-a
    def run_filter_instructors_view(self) -> None:
        self.filter_instructors_win.mainloop()
