from tkinter import *


class ReservationsInstructorView:
    # Konstruktor
    def __init__(self, root, reservations):
        # Pregled rezervacija - za instruktora
        self.reservations_win = Toplevel(root)
        self.reservations_win.resizable(False, False)
        self.reservations_win.geometry("550x400")
        self.reservations_win.title("Instruction Network - Rezervirani Termini")
        self.reservations_win.iconbitmap("../images/instruction_network_icon.ico")
        self.reservations_win.configure(bg="#0F4C81")

        self.title_label = Label(self.reservations_win, text="REZERVIRANI TERMINI",
                                 bg="#0F4C81", font=("Courier", 14, "bold"))
        self.title_label.grid(row=0, column=0, columnspan=2, sticky=W + E)

        if len(reservations) != 0:
            # Ako instruktor ima rezervirane termina
            self.cancel_label = Label(self.reservations_win, text="Kliknite na termin da biste poništili rezervaciju!",
                                      bg="#0F4C81", font=("Courier", 10, "bold"))
            self.cancel_label.grid(row=1, column=0, columnspan=2, sticky=W + E)

            # Frame - Rezervacije
            self.reservations_frame = Frame(self.reservations_win, bg="#0F4C81", padx=20, pady=20)
            self.reservations_frame.grid(row=2, column=0)

            # Scrollbar - Rezervacije
            self.scrollbar_reservations = Scrollbar(self.reservations_frame)
            self.scrollbar_reservations.pack(side=RIGHT, fill=Y)
            self.reservations_listbox = Listbox(self.reservations_frame,
                                                yscrollcommand=self.scrollbar_reservations.set, bg="#0F4C81",
                                                font=("Courier", 10), width=60, selectmode="single", height=15)
            self.scrollbar_reservations.config(command=self.reservations_listbox.yview)

            # Rezervacije
            for r in reservations:
                online = ""
                location = r.get_appointment().get_location()
                if r.get_appointment().get_is_online():
                    online = "online"
                    location = ""
                tmp = str(reservations.index(r) + 1) + ". " + r.get_student().get_first_name() + " " + \
                      r.get_student().get_last_name() + ", " + r.get_appointment().get_date() + ", " + \
                      r.get_appointment().get_start_time() + "-" + r.get_appointment().get_end_time() + \
                      ", " + online + location
                self.reservations_listbox.insert(END, tmp)
                self.reservations_listbox.pack()

            # Button - Poništavanje rezervacije
            self.cancel_button = Button(self.reservations_win, text="PONIŠTI REZERVACIJU",
                                        font=("Courier", 12),
                                        width=20, bg="#658DC6")
            self.cancel_button.grid(row=3, column=0, columnspan=2)
        else:
            # Ako instruktor nema niti jedan rezerviran termin
            self.else_label = Label(self.reservations_win, text="Nemate nijednu rezervaciju.",
                                      bg="#0F4C81", font=("Courier", 10, "bold"))
            self.else_label.grid(row=1, column=0, columnspan=2, sticky=W + E)

    # Prikaz views-a
    def run_reservations_instructor_view(self) -> None:
        self.reservations_win.mainloop()

    # Update-ovanje views-a
    def update_reservations_instructor_view(self, reservations) -> None:
        self.reservations_listbox.delete(0, END)
        for r in reservations:
            online = ""
            location = r.get_appointment().get_location()
            if r.get_appointment().get_is_online():
                online = "online"
                location = ""
            tmp = str(reservations.index(r) + 1) + ". " + r.get_student().get_first_name() + " " + \
                  r.get_student().get_last_name() + ", " + r.get_appointment().get_date() + ", " + \
                  r.get_appointment().get_start_time() + "-" + r.get_appointment().get_end_time() + \
                  ", " + online + location
            self.reservations_listbox.insert(END, tmp)
            self.reservations_listbox.pack()
