from tkinter import *
from PIL import ImageTk, Image
from views.RegisterUserView import RegisterUserView


class EditAccountView(RegisterUserView):
    # Konstruktor
    def __init__(self, root, subjects, users_subjects):
        RegisterUserView.__init__(self, root, subjects)
        # Uredi profil
        self.edit_acc_win = self.register_win
        self.edit_acc_win.title("Instruction Network - Uredi Profil")
        self.subjects_label.configure(text="Izabrani predmeti: ")
        self.register_button.configure(text="UREDI")



        # Selektovanje prethodno izabranih predmeta
        tmp = self.subjects_listbox.get(0, END)
        for ss in users_subjects:
            for t in tmp:
                if t == ss:
                    self.subjects_listbox.selection_set(tmp.index(t), None)

    # Prikaz View-a
    def run_edit_acc_win(self) -> None:
        self.edit_acc_win.mainloop()
