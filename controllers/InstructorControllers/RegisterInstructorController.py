from __future__ import annotations
from views.Facade import *
from models.InstructorCreator import InstructorCreator
from util.Validations import *
from models.DatabaseConnection import DatabaseConnection

db = DatabaseConnection()


# prikuplja podatke o korisniku
def collect_users_data(view) -> ():
    fname = view.first_name_entry.get()
    lname = view.last_name_entry.get()
    email = view.email_entry.get()
    username = view.username_entry.get()
    password = view.password_entry.get()
    image = None
    if view.has_image is True:
        with open(view.register_win.filename, "rb") as img:
            image = img.read()
    subjects = [view.subjects_listbox.get(idx) for idx in view.subjects_listbox.curselection()]
    is_ok = validate_users_data(fname, lname, email, username, password, subjects)
    if is_ok:
        return None, fname, lname, email, username, password, image, subjects


# upisuje instruktora u bazu
def submit_register_info() -> None:
    global instructor
    data = collect_users_data(register_view)
    if data is not None:
        instructor_creator = InstructorCreator()
        instructor = instructor_creator.factory_method(data[0], data[1], data[2], data[3], data[4], data[5], data[6],
                                                       data[7])
        instructor_id = db.insert_into_instructors(instructor)
        if instructor_id == -1:
            messagebox.showerror("Greška", "Već postoji registriran korisnik sa istim email-om ili username-om!")
        else:
            instructor.set_user_id(instructor_id)
            register_view.register_win.destroy()
            facade.login_view.login_win.deiconify()


# prikazuje RegisterInstructorView i pridruzuje mu metode
def run_register_instructor_view(f: Facade) -> None:
    global facade
    facade = f
    global register_view
    subjects = db.get_subjects_names()
    register_view = facade.get_register_instructor_view(subjects)
    facade.attach_button_command(register_view.register_button, submit_register_info)
    register_view.run_register_view()
