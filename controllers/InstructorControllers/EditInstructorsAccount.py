from models.DatabaseConnection import DatabaseConnection
from models.Instructor import Instructor
from views.Facade import Facade
from numpy import unique
from util.Validations import *

db = DatabaseConnection()


# prikuplja nove podatke o korisniku
def collect_new_users_data(view, user) -> ():
    fname = user.get_first_name()
    lname = user.get_last_name()
    email = user.get_email()
    username = user.get_username()
    password = user.get_password()
    image = user.get_image()
    subjects = user.get_subjects()
    if user.get_first_name() != view.first_name_entry.get():
        fname = view.first_name_entry.get()
    if user.get_last_name() != view.last_name_entry.get():
        lname = view.last_name_entry.get()
    if user.get_email() != view.email_entry.get():
        email = view.email_entry.get()
    if user.get_username() != view.username_entry.get():
        username = view.username_entry.get()
    if user.get_password() != view.password_entry.get():
        password = view.password_entry.get()
    if view.has_image is True:
        with open(view.register_win.filename, "rb") as img:
            new_image = img.read()
        if user.get_image() != new_image:
            image = new_image
    new_subjects = [view.subjects_listbox.get(idx) for idx in view.subjects_listbox.curselection()]
    if user.get_subjects() != new_subjects:
        subjects = new_subjects
    is_ok = validate_users_data(fname, lname, email, username, password, subjects)
    if is_ok:
        return None, fname, lname, email, username, password, image, subjects


# prepisuje stare podatke novim u bazi
def submit_info_changes() -> None:
    data = collect_new_users_data(edit_account_view, instructor)
    if data is not None:
        instructor.set_first_name(data[1])
        instructor.set_last_name(data[2])
        instructor.set_email(data[3])
        instructor.set_username(data[4])
        instructor.set_password(data[5])
        instructor.set_image(data[6])
        instructor.set_subjects(unique(data[7]).tolist())
        db.update_instructors_data(instructor)
        facade.notify_homepage_instructor_view(instructor)
        edit_account_view.edit_acc_win.destroy()


# prikazuje EditAccountView i pridruzuje metode button-ima
def edit_users_info(f: Facade, i: Instructor) -> None:
    global edit_account_view
    global facade
    global instructor
    facade = f
    instructor = i
    subjects = db.get_subjects_names()
    edit_account_view = facade.get_edit_account_view(instructor, subjects)
    facade.attach_button_command(edit_account_view.register_button, submit_info_changes)
    edit_account_view.run_edit_acc_win()


# brise podatke o instruktoru iz baze
def delete_account(f: Facade, i: Instructor) -> None:
    facade = f
    instructor = i
    u_id = instructor.get_user_id()
    ans = messagebox.askyesnocancel("Brisanje računa", "Da li ste sigurni da želite obrisati svoj korisnički račun?")
    if ans is True:
        db.delete_instructor(u_id)
        facade.delete_instructors_account()
