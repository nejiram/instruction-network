from models.DatabaseConnection import DatabaseConnection
from models.Instructor import Instructor
from views.Facade import Facade
from controllers.InstructorControllers.CreatedAppointmentsController import view_created_appointments
from controllers.InstructorControllers.ReservationsInstructorController import view_reservations
from controllers.InstructorControllers.CreateAppointmentController import create_appointment
from controllers.InstructorControllers.EditInstructorsAccount import edit_users_info, delete_account


# prikazuje HomepageInstructorView i pridruzuje metode button-ima
def run_homepage_instructor_view(f: Facade, user: Instructor) -> None:
    instructor = user
    facade = f
    global homepage_instructor_view
    homepage_instructor_view = facade.get_homepage_instructor_view(instructor)
    facade.attach_button_command(homepage_instructor_view.view_appointments_button,
                                 lambda: view_created_appointments(facade, instructor))
    facade.attach_button_command(homepage_instructor_view.view_reservations_button,
                                 lambda: view_reservations(facade, instructor))
    facade.attach_button_command(homepage_instructor_view.create_appointment_button,
                                 lambda: create_appointment(facade, instructor))
    facade.attach_button_command(homepage_instructor_view.edit_account_button,
                                 lambda: edit_users_info(facade, instructor))
    facade.attach_button_command(homepage_instructor_view.delete_account_button,
                                 lambda: delete_account(facade, instructor))
    homepage_instructor_view.run_homepage_instructor_view()
