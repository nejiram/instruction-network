from models.DatabaseConnection import DatabaseConnection
from models.Instructor import Instructor
from views.Facade import Facade
from util.Validations import *
from models.Appointment import Appointment
from tkinter import messagebox

db = DatabaseConnection()


# prikuplja podatke o terminu
def collect_appointment_data() -> ():
    date = create_appointment_view.date_entry.get()
    start_time = create_appointment_view.start_time_entry.get()
    end_time = create_appointment_view.end_time_entry.get()
    is_online = create_appointment_view.online_var.get()
    subject_index = create_appointment_view.subjects_listbox.curselection()
    location = create_appointment_view.location_value.get()
    price = create_appointment_view.price_entry.get()
    is_ok = validate_appointment(subject_index, date, start_time, end_time, is_online, location, price)
    if is_ok:
        subject = create_appointment_view.subjects_listbox.get(create_appointment_view.subjects_listbox.curselection())
        return subject, date, start_time, end_time, is_online, location, price


# upisuje novokreirani termin u bazu
def submit_appointment() -> None:
    data = collect_appointment_data()
    if data is not None:
        new_appointment = Appointment(None, instructor, data[0], data[1], data[2], data[3], data[4], data[5], data[6])
        ok = db.insert_into_appointments(new_appointment)
        if ok == 1:
            create_appointment_view.create_appointment_win.destroy()
        elif ok == -1:
            messagebox.showerror("Greška", "Uneseno vrijeme termina se poklapa sa već postojećim terminom!")


# prikazuje CreateAppointmentView i pridruzuje metode button-ima
def create_appointment(f: Facade, i: Instructor) -> None:
    global create_appointment_view
    global facade
    global instructor
    facade = f
    instructor = i
    create_appointment_view = facade.get_create_appointment_view(instructor.get_subjects(), db.get_locations())
    facade.attach_button_command(create_appointment_view.create_button, submit_appointment)
    create_appointment_view.run_created_appointment_view()
