from models.DatabaseConnection import DatabaseConnection
from models.Instructor import Instructor
from views.Facade import *
from tkinter import messagebox

db = DatabaseConnection()


# sortira termine na one koji su prosli i na predstojece
def past_future_appointments(appointments):
    past_appointments = []
    future_appointments = []
    day, month, year = db.get_todays_dd_mm_yyyy()
    for a in appointments:
        dd, mm, yyyy = db.date_to_dd_mm_yyyy(a.get_date())
        if yyyy < year or (yyyy == year and mm < month) or (yyyy == year and mm == month and dd < day):
            past_appointments.append(a)
        elif yyyy == year and mm == month and dd == day:
            now = db.current_time_to_seconds()
            end_time = db.time_to_sec(a.get_end_time())
            if end_time <= now:
                past_appointments.append(a)
            else:
                future_appointments.append(a)
        else:
            future_appointments.append(a)
    return past_appointments, future_appointments


# prikuplja podatke o terminu
def get_appointments_data(item):
    item_data = created_appointments_view.future_appointments_listbox.get(item[0])
    data = item_data.split(", ")
    a_subject = data[0].split(": ")[1]
    a_date = data[1].split(": ")[1]
    a_start = data[2].split(": ")[1]
    a_end = data[3].split(": ")[1]
    a_online = 0
    a_location = "Sarajevo"
    if data[4].split(": ")[1] == "da":
        a_online = 1
    else:
        a_location = data[5].split(": ")[1]
    a_price = int(data[6].split(": ")[1])
    return a_subject, a_date, a_start, a_end, a_online, a_location, a_price


# pronalazi rezervaciju u listi rezervacija
def find_reservation(item):
    a_subject, a_date, a_start, a_end, a_online, a_location, a_price = get_appointments_data(item)
    reservations = db.get_instructors_reservations(instructor.get_user_id())
    for r in reservations:
        a = r.get_appointment()
        r_subject = a.get_subject()
        r_date = a.get_date()
        r_start = a.get_start_time()
        r_end = a.get_end_time()
        r_online = a.get_is_online()
        r_location = a.get_location()
        r_price = a.get_price()
        if a_subject == r_subject and a_date == r_date and a_start == r_start and a_end == r_end and \
                a_online == r_online and a_location == r_location and a_price == r_price:
            return r
    return None


# pronalazi termin u listi termina
def find_appointment(item):
    a_subject, a_date, a_start, a_end, a_online, a_location, a_price = get_appointments_data(item)
    for a in appointments:
        if a_subject == a.get_subject() and a_date == a.get_date() and a_start == a.get_start_time() and \
                a_end == a.get_end_time() and (a_online == a.get_is_online() or a_location == a.get_location()) and \
                a_price == a.get_price():
            return a


# brise termin iz baze
def delete_appointment() -> None:
    item = created_appointments_view.future_appointments_listbox.curselection()
    if len(item) != 0:
        r = find_reservation(item)
        if r is None:
            a = find_appointment(item)
            db.delete_from_appointments(a.get_appointment_id())
        else:
            ans = messagebox.askyesnocancel("Upozorenje",
                                            "Termin koji ste izabrali je rezerviran. Da li ga želite otkazati?")
            if ans:
                db.delete_from_reservations(r)
        appointments = db.get_created_appointments(instructor.get_user_id())
        past_appointments, future_appointments = past_future_appointments(appointments)
        facade.notify_created_appointments_view(future_appointments)


# prikazuje CreatedAppointmentsView i pridruzuje metode button-ima
def view_created_appointments(f: Facade, i: Instructor) -> None:
    global appointments
    global facade
    global instructor
    facade = f
    instructor = i
    appointments = db.get_created_appointments(instructor.get_user_id())
    past_appointments, future_appointments = past_future_appointments(appointments)
    global created_appointments_view
    created_appointments_view = facade.get_created_appointments_view(past_appointments, future_appointments)
    facade.attach_button_command(created_appointments_view.delete_appointment_button, delete_appointment)
    created_appointments_view.run_created_appointments_view()
