from models.DatabaseConnection import DatabaseConnection
from models.Instructor import Instructor
from views.Facade import Facade
from tkinter import messagebox

db = DatabaseConnection()


def get_reservation(item):
    item_data = reservations_instructor_view.reservations_listbox.get(item[0])
    data = item_data.split(", ")
    r_student = data[0].split(". ")[1]
    r_date = data[1]
    r_start = data[2].split("-")[0]
    r_end = data[2].split("-")[1]
    r_online = ""
    r_location = ""
    if data[3] == "online":
        r_online = 1
    else:
        r_location = data[3]
    reservations = db.get_instructors_reservations(instructor.get_user_id())
    for r in reservations:
        a_student = r.get_student().get_first_name() + " " + r.get_student().get_last_name()
        a_date = r.get_appointment().get_date()
        a_start = r.get_appointment().get_start_time()
        a_end = r.get_appointment().get_end_time()
        a_online = r.get_appointment().get_is_online()
        a_location = r.get_appointment().get_location()
        if r_student == a_student and r_date == a_date and r_start == a_start and r_end == a_end:
            if r_online == a_online:
                return r
            elif r_location == a_location:
                return r


def cancel_reservation():
    item = reservations_instructor_view.reservations_listbox.curselection()
    if len(item) != 0:
        r = get_reservation(item)
        ok = db.delete_from_reservations(r)
        if ok == 0:
            messagebox.showerror("Greška", "Nije moguće poništiti rezervacije koje su prošle!")
        elif ok == -1:
            messagebox.showerror("Greška", "Rezervaciju je moguće poništiti najkasnije 24 sata prije početka termina!")
        elif ok == 1:
            reservations = db.get_instructors_reservations(instructor.get_user_id())
            facade.notify_reservations_instructor_view(reservations)


def view_reservations(f: Facade, i: Instructor) -> None:
    global facade
    global instructor
    facade = f
    instructor = i
    reservations = db.get_instructors_reservations(instructor.get_user_id())
    global reservations_instructor_view
    reservations_instructor_view = facade.get_reservations_instructor_view(reservations)
    if len(reservations) != 0:
        facade.attach_button_command(reservations_instructor_view.cancel_button, cancel_reservation)
    reservations_instructor_view.run_reservations_instructor_view()
