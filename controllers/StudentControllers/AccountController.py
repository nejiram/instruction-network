from models.DatabaseConnection import DatabaseConnection
from views.Facade import *
from models.Student import Student
from numpy import unique
from controllers.InstructorControllers.EditInstructorsAccount import collect_new_users_data
from tkinter import messagebox

db = DatabaseConnection()


# prepisuje stare podatke novim u bazi
def submit_info_changes() -> None:
    data = collect_new_users_data(edit_account_view, student)
    if data is not None:
        student.set_first_name(data[1])
        student.set_last_name(data[2])
        student.set_email(data[3])
        student.set_username(data[4])
        student.set_password(data[5])
        student.set_image(data[6])
        student.set_subjects(unique(data[7]).tolist())
        db.update_students_data(student)
        facade.notify_student_account_view(student)
        appointments = db.get_all_available_appointments(student.get_user_id(), student.get_subjects())
        facade.notify_homepage_student_view(appointments)
        edit_account_view.edit_acc_win.destroy()


# prikazuje EditAccountView i pridruzuje metode button-ima
def edit_users_info() -> None:
    global edit_account_view
    subjects = db.get_subjects_names()
    edit_account_view = facade.get_edit_account_view(student, subjects)
    facade.attach_button_command(edit_account_view.register_button, submit_info_changes)
    edit_account_view.run_edit_acc_win()


# brise podatke o instruktoru iz baze
def delete_account() -> None:
    u_id = student.get_user_id()
    ans = messagebox.askyesnocancel("Brisanje računa", "Da li ste sigurni da želite obrisati svoj korisnički račun?")
    if ans is True:
        db.delete_student(u_id)
        my_account_view.account_win.destroy()
        facade.delete_account()


# prikazuje AccountView i pridruzuje metode button-ima
def view_my_account(f: Facade, s: Student) -> None:
    global my_account_view
    global facade
    global student
    student = s
    facade = f
    my_account_view = facade.get_my_account_student_view(student)
    facade.attach_button_command(my_account_view.edit_button, edit_users_info)
    facade.attach_button_command(my_account_view.delete_button, delete_account)
    my_account_view.run_my_account_view()
