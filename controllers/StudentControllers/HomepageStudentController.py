from models.DatabaseConnection import DatabaseConnection
from models.Student import Student
from views.Facade import *
from controllers.StudentControllers.FilterInstructorsController import filter_instructors
from controllers.StudentControllers.ReservationsStudentController import view_reservations
from controllers.StudentControllers.AccountController import view_my_account
from controllers.StudentControllers.InstructorReviewController import review_instructor

db = DatabaseConnection()


# prikazuje HomepageStudentView i pridruzuje metode button-ima
def run_homepage_student_view(f: Facade, user: Student) -> None:
    facade = f
    student = user
    appointments = db.get_all_available_appointments(student.get_user_id(), student.get_subjects())
    homepage_student_view = facade.get_homepage_student_view(appointments)
    facade.attach_button_command(homepage_student_view.filter_button,
                                 lambda: filter_instructors(facade, student, appointments))
    facade.attach_button_command(homepage_student_view.reservations_button, lambda: view_reservations(facade, student))
    facade.attach_button_command(homepage_student_view.account_button, lambda: view_my_account(facade, student))
    facade.attach_button_command(homepage_student_view.show_instructor_button,
                                 lambda: review_instructor(facade, homepage_student_view, student))
    homepage_student_view.run_homepage_student_view()
