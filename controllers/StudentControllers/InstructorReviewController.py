from models.DatabaseConnection import DatabaseConnection
from models.Student import Student
from views.Facade import *
from models.Instructor import Instructor
from tkinter import messagebox
from models.Appointment import Appointment

db = DatabaseConnection()


# upisuje rezervaciju u bazu
def reserve(instructor) -> None:
    item = instructor_review_view.appointments_listbox.curselection()
    item_data = instructor_review_view.appointments_listbox.get(item[0])
    data = item_data.split(", ")
    subject_name = data[0]
    appointment_date = data[1]
    appointment_time = data[2].split("-")
    start_time = appointment_time[0]
    end_time = appointment_time[1]
    online_or_location = data[3]
    is_online = False
    location = "--"
    if online_or_location == "online":
        is_online = True
    else:
        location = online_or_location
    price = data[4]
    appointment = Appointment(None, instructor, subject_name, appointment_date, start_time, end_time, is_online,
                              location, price)
    ok = db.insert_into_reservations(student, appointment)
    if ok == 1:
        messagebox.showinfo("Info", "Termin rezerviran!")
        appointments1 = db.get_instructors_appointments(instructor.get_user_id(), student.get_user_id(),
                                                        student.get_subjects())
        facade.notify_appointments_instructor_review_view(appointments1)
        appointments = db.get_all_available_appointments(student.get_user_id(), student.get_subjects())
        facade.notify_homepage_student_view(appointments)
    elif ok == -1:
        messagebox.showerror("Greška", "Imate rezerviran termin u isto vrijeme!")


# upisuje ocjenu za instruktora u bazu
def rate_instructor(instructor) -> None:
    grade = instructor_review_view.grade_value.get()
    ok = db.check_ratings(instructor.get_user_id(), student.get_user_id())
    if ok == 1:
        err = db.rate_instructor(instructor.get_user_id(), student.get_user_id(), grade)
        if err == 0:
            messagebox.showerror("Greška", "Niste imali nijedan termin kod ovog instruktora!")
        elif err == -1:
            messagebox.showerror("Greška",
                                 "Mora proći najmanje 24 sata od kraja rezervacije kako biste mogli ocijeniti instruktora!")
        elif err == 1:
            instructor.add_rating(grade)
            instructor.set_ratings(db.get_instructors_ratings(instructor.get_user_id()))
            facade.notify_rating_instructor_review_view(instructor.get_average_rating())
    elif ok == -1:
        ans = messagebox.askyesnocancel("Ocjenjivanje",
                                        "Već ste ocijenili ovog instruktora. Želite li promijeniti ocjenu?")
        if ans:
            db.change_grade(instructor.get_user_id(), student.get_user_id(), grade)
            instructor.set_ratings(db.get_instructors_ratings(instructor.get_user_id()))
            facade.notify_rating_instructor_review_view(instructor.get_average_rating())


# prikazuje InstructorReviewView i pridruzuje metode button-ima
def review_instructor(f: Facade, homepage_student_view: HomepageStudentView, s: Student) -> Instructor:
    global facade
    global student
    facade = f
    student = s
    item = homepage_student_view.instructors_listbox.curselection()
    item_data = homepage_student_view.instructors_listbox.get(item[0])
    data = item_data.split(", ")
    username = data[0][1:]
    instructor = db.get_instructor_by_username(username)
    global instructor_review_view
    a = db.get_instructors_appointments(instructor.get_user_id(), student.get_user_id(),
                                        student.get_subjects())
    instructor_review_view = facade.get_instructor_review_view(instructor, db.get_ratings(), a)
    facade.attach_button_command(instructor_review_view.reserve_button, lambda: reserve(instructor))
    facade.attach_button_command(instructor_review_view.rate_button, lambda: rate_instructor(instructor))
    instructor_review_view.run_instructor_review_view()
