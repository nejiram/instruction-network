from models.DatabaseConnection import DatabaseConnection
from models.Student import Student
from views.Facade import *
from tkinter import messagebox

db = DatabaseConnection()


# vraca odabranu rezervaciju iz liste rezervacija
def get_reservation():
    if len(reservations_student_view.reservations_listbox.curselection()) != 0:
        reservation_for_cancel = reservations_student_view.reservations_listbox.get(
            reservations_student_view.reservations_listbox.curselection())
        print(reservation_for_cancel)
        reservation_data = reservation_for_cancel.split(". ")
        reservation_data = reservation_data[1].split(", ")
        r_name = reservation_data[0]
        r_subject = reservation_data[1]
        r_date = reservation_data[2]
        r_start_time = int(reservation_data[3][:2]) * 3600 + int(reservation_data[3][3:5]) * 60
        r_end_time = int(reservation_data[3][6:8]) * 3600 + int(reservation_data[3][9:]) * 60
        r_online = ""
        r_location = ""
        if reservation_data[4] == "online":
            r_online = 1
        else:
            r_location = reservation_data[4]
        for r in reservations:
            i_name = r.get_appointment().get_instructor().get_first_name() + " " + r.get_appointment().get_instructor().get_last_name()
            a_subject = r.get_appointment().get_subject()
            a_date = r.get_appointment().get_date()
            a_start_time = int(r.get_appointment().get_start_time()[:2]) * 3600 + int(
                r.get_appointment().get_start_time()[3:]) * 60
            a_end_time = int(r.get_appointment().get_end_time()[:2]) * 3600 + int(
                r.get_appointment().get_end_time()[3:]) * 60
            if r_name == i_name and r_subject == a_subject and r_date == a_date and r_start_time == a_start_time and r_end_time == a_end_time:
                if r_online == r.get_appointment().get_is_online():
                    return r
                elif r_location == r.get_appointment().get_location():
                    return r


# otkazuje rezervaciju - brise je iz baze
def cancel_reservation() -> None:
    r = get_reservation()
    if r is not None:
        ok = db.delete_from_reservations(r)
        if ok == 0:
            messagebox.showerror("Greška", "Nije moguće poništiti rezervacije koje su prošle!")
        elif ok == -1:
            messagebox.showerror("Greška", "Rezervaciju je moguće poništiti najkasnije 24 sata prije početka termina!")
        elif ok == 1:
            reservations = db.get_students_reservations(student)
            facade.notify_reservations_student_view(reservations)
            appointments = db.get_all_available_appointments(student.get_user_id(), student.get_subjects())
            facade.notify_homepage_student_view(appointments)


# prikazuje ReservationsStudentView i pridruzuje metode button-ima
def view_reservations(f: Facade, s: Student) -> None:
    global reservations
    global facade
    facade = f
    global student
    student = s
    reservations = db.get_students_reservations(student)
    global reservations_student_view
    reservations_student_view = facade.get_reservations_student_view(reservations)
    if len(reservations) != 0:
        facade.attach_button_command(reservations_student_view.cancel_button, cancel_reservation)
    reservations_student_view.run_reservations_student_view()
