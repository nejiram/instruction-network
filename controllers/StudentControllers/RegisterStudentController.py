from __future__ import annotations
from views.Facade import *
from models.StudentCreator import StudentCreator
from tkinter import messagebox
from models.DatabaseConnection import DatabaseConnection
from controllers.InstructorControllers.RegisterInstructorController import collect_users_data

db = DatabaseConnection()


# upisuje studenta u bazu
def submit_register_info() -> None:
    data = collect_users_data(register_view)
    if data is not None:
        student_creator = StudentCreator()
        student = student_creator.factory_method(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7])
        student_id = db.insert_into_students(student)
        if student_id == -1:
            messagebox.showerror("Greška", "Već postoji registriran korisnik sa istim email-om ili username-om!")
        else:
            student.set_user_id(student_id)
            register_view.register_win.destroy()
            facade.login_view.login_win.deiconify()


# prikazuje RegisterStudentView i pridruzuje mu metode
def run_register_student_view(f: Facade) -> None:
    global facade
    facade = f
    global register_view
    subjects = db.get_subjects_names()
    register_view = facade.get_register_student_view(subjects)
    facade.attach_button_command(register_view.register_button, submit_register_info)
    register_view.run_register_view()
