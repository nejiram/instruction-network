from models.DatabaseConnection import DatabaseConnection
from views.Facade import *
from models.Student import Student
from util.Validations import *
from tkinter import messagebox

db = DatabaseConnection()


# resetuje filtere
def reset_filters() -> None:
    facade.notify_homepage_student_view(appointments)
    filter_instructors_view.filter_instructors_win.destroy()


# pomocna funkcija - filtrira termine po predmetima
def filter_by_subjects(subjects) -> []:
    new_appointments = []
    for a in appointments:
        for s in subjects:
            if a.get_subject() == s:
                new_appointments.append(a)
    return new_appointments


# pomocna funkcija - filtrira termine po datumu
def filter_by_date(app, start_date, end_date) -> []:
    new_appointments = []
    ok = True
    if start_date != "" and end_date != "":
        if validate_date(start_date) and validate_date(end_date):
            dd_start = int(start_date[:2])
            mm_start = int(start_date[3:5])
            yyyy_start = int(start_date[6:])
            dd_end = int(end_date[:2])
            mm_end = int(end_date[3:5])
            yyyy_end = int(end_date[6:])
            for a in app:
                dd = int(a.get_date()[:2])
                mm = int(a.get_date()[3:5])
                yyyy = int(a.get_date()[6:])
                if yyyy_start <= yyyy <= yyyy_end and mm_start <= mm <= mm_end and dd_start <= dd <= dd_end:
                    new_appointments.append(a)
        else:
            messagebox.showerror("Greška",
                                 "Datum mora biti u formatu \"dd.mm.yyyy\" ili \"dd/mm/yyyy\" ili \"dd-mm-yyyy\".")
            ok = False
    elif start_date != "":
        if validate_date(start_date):
            dd_start = int(start_date[:2])
            mm_start = int(start_date[3:5])
            yyyy_start = int(start_date[6:])
            for a in app:
                dd = int(a.get_date()[:2])
                mm = int(a.get_date()[3:5])
                yyyy = int(a.get_date()[6:])
                if yyyy_start <= yyyy and mm_start <= mm and dd_start <= dd:
                    new_appointments.append(a)
        else:
            messagebox.showerror("Greška",
                                 "Datum mora biti u formatu \"dd.mm.yyyy\" ili \"dd/mm/yyyy\" ili \"dd-mm-yyyy\".")
            ok = False
    elif end_date != "":
        if validate_date(end_date):
            dd_end = int(end_date[:2])
            mm_end = int(end_date[3:5])
            yyyy_end = int(end_date[6:])
            for a in app:
                dd = int(a.get_date()[:2])
                mm = int(a.get_date()[3:5])
                yyyy = int(a.get_date()[6:])
                if yyyy_end >= yyyy and mm_end >= mm and dd_end >= dd:
                    new_appointments.append(a)
        else:
            messagebox.showerror("Greška",
                                 "Datum mora biti u formatu \"dd.mm.yyyy\" ili \"dd/mm/yyyy\" ili \"dd-mm-yyyy\".")
            ok = False
    if ok:
        return new_appointments


# pomocna funkcija - filtrira termine po vremenu
def filter_by_time(app, start_time, end_time) -> []:
    new_appointments = []
    ok = True
    if start_time != "" and end_time != "":
        if validate_start_time(start_time) and validate_end_time(end_time):
            start_sec = int(start_time[:2]) * 3600 + int(start_time[3:]) * 60
            end_sec = int(end_time[:2]) * 3600 + int(end_time[3:]) * 60
            for a in app:
                sec_s = int(a.get_start_time()[:2]) * 3600 + int(a.get_start_time()[3:])
                sec_e = int(a.get_end_time()[:2]) * 3600 + int(a.get_end_time()[3:]) * 60
                if start_sec <= sec_s <= sec_e <= end_sec:
                    new_appointments.append(a)
        else:
            messagebox.showerror("Greška", "Vrijeme kraja termina mora biti u formatu \"hh:mm\".")
            ok = False
    elif start_time != "":
        if validate_start_time(start_time):
            start_sec = int(start_time[:2]) * 3600 + int(start_time[3:]) * 60
            for a in app:
                sec = int(a.get_start_time()[:2]) * 3600 + int(a.get_start_time()[3:]) * 60
                if sec >= start_sec:
                    new_appointments.append(a)
        else:
            messagebox.showerror("Greška", "Vrijeme kraja termina mora biti u formatu \"hh:mm\".")
            ok = False
    elif end_time != "":
        if validate_end_time(end_time):
            end_sec = int(end_time[:2]) * 3600 + int(end_time[3:]) * 60
            for a in app:
                sec = int(a.get_end_time()[:2]) * 3600 + int(a.get_end_time()[3:]) * 60
                if sec <= end_sec:
                    new_appointments.append(a)
        else:
            messagebox.showerror("Greška", "Vrijeme kraja termina mora biti u formatu \"hh:mm\".")
            ok = False
    if ok:
        return new_appointments


# pomocna funkcija - filtrira online termine
def filter_online(app) -> []:
    new_appointments = []
    for a in app:
        if a.get_is_online():
            new_appointments.append(a)
    return new_appointments


# pomocna funkcija - filtrira termine po lokaciji
def filter_by_location(app, location) -> []:
    new_appointments = []
    for a in app:
        if a.get_location() == location:
            new_appointments.append(a)
    return new_appointments


# pomocna funkcija - filtrira termine po cijeni
def filter_by_price(app, from_price, to_price) -> []:
    new_appointments = []
    for a in app:
        if from_price != "" and to_price != "" and int(from_price) <= a.get_price() <= int(to_price):
            new_appointments.append(a)
        elif from_price != "" and to_price == "" and a.get_price() >= int(from_price):
            new_appointments.append(a)
        elif from_price == "" and to_price != "" and a.get_price() <= int(to_price):
            new_appointments.append(a)
    return new_appointments


# filtriranje termina
def set_filters() -> None:
    new_appointments = []
    subjects = [filter_instructors_view.subjects_listbox.get(idx) for idx in
                filter_instructors_view.subjects_listbox.curselection()]
    if len(subjects) != 0:
        new_appointments = filter_by_subjects(subjects)

    start_date = filter_instructors_view.from_date_entry.get()
    end_date = filter_instructors_view.to_date_entry.get()
    if start_date != "" or end_date != "":
        if len(new_appointments) != 0:
            new_appointments = filter_by_date(new_appointments, start_date, end_date)
        else:
            new_appointments = filter_by_date(appointments, start_date, end_date)

    start_time = filter_instructors_view.from_time_entry.get()
    end_time = filter_instructors_view.to_time_entry.get()
    if start_time != "" or end_time != "":
        if len(new_appointments) != 0:
            new_appointments = filter_by_time(new_appointments, start_time, end_time)
        else:
            new_appointments = filter_by_time(appointments, start_time, end_time)

    is_online = filter_instructors_view.online_var.get()
    if is_online:
        if len(new_appointments) != 0:
            new_appointments = filter_online(new_appointments)
        else:
            new_appointments = filter_online(appointments)

    location = filter_instructors_view.location_value.get()
    if location != "--":
        if len(new_appointments) != 0:
            new_appointments = filter_by_location(new_appointments, location)
        else:
            new_appointments = filter_by_location(appointments, location)

    from_price = filter_instructors_view.from_price_entry.get()
    to_price = filter_instructors_view.to_price_entry.get()
    if from_price != "" or to_price != "":
        if len(new_appointments) != 0:
            new_appointments = filter_by_price(new_appointments, from_price, to_price)
        else:
            new_appointments = filter_by_price(appointments, from_price, to_price)

    if isinstance(new_appointments, list):
        new_appointments = db.sort_appointments(new_appointments)
        facade.notify_homepage_student_view(new_appointments)
        filter_instructors_view.filter_instructors_win.destroy()


# prikazuje FilterInstructorsView i pridruzuje metode button-ima
def filter_instructors(f: Facade, student: Student, appointments_list: []) -> None:
    global filter_instructors_view
    global facade
    global appointments
    appointments = appointments_list
    facade = f
    filter_instructors_view = facade.get_filter_instructors_view(student.get_subjects())
    facade.attach_button_command(filter_instructors_view.filter_button, set_filters)
    facade.attach_button_command(filter_instructors_view.reset_button, reset_filters)
    filter_instructors_view.run_filter_instructors_view()
