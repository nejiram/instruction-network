from models.DatabaseConnection import DatabaseConnection
from controllers.InstructorControllers.RegisterInstructorController import run_register_instructor_view
from controllers.StudentControllers.RegisterStudentController import run_register_student_view
from controllers.StudentControllers.HomepageStudentController import run_homepage_student_view
from controllers.InstructorControllers.HomepageInstructorController import run_homepage_instructor_view
from views.Facade import *
from tkinter import messagebox


# registracija korisnika
def register_user_btn():
    ans = messagebox.askyesnocancel("Registracija",
                                    "Želite li se registrirati kao instruktor ili kao student ? \n"
                                    "(Kliknite \"Yes\" ako se želite registrirati kao instruktor, ili kliknite \"No\" "
                                    "ako se želite registrirati kao student.)")
    if ans is True:
        run_register_instructor_view(facade)
    elif ans is False:
        run_register_student_view(facade)


# login korisnika
def login_btn():
    username = login_view.username_entry.get()
    password = login_view.password_entry.get()
    user = db.get_login_data(username, password)
    if user is None:
        messagebox.showerror("Greška", "Pogrešan username ili password!")
    elif user[0] == "student":
        run_homepage_student_view(facade, user[1])
    elif user[0] == "instructor":
        run_homepage_instructor_view(facade, user[1])


# main
def run():
    global db
    global login_view
    global subjects
    global facade
    db = DatabaseConnection()
    subjects = db.get_subjects_names()
    login_view = LoginView()
    facade = Facade(login_view)
    facade.attach_button_command(login_view.login_button, login_btn)
    facade.attach_button_command(login_view.register_button, register_user_btn)
    login_view.run()


if __name__ == "__main__":
    run()
