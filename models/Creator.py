# factory method
from __future__ import annotations
from abc import ABC, abstractmethod


class Creator(ABC):

    @abstractmethod
    def factory_method(self, user_id: int, fname: str, lname: str, email: str, username: str, password: str,
                       image: bytes, subjects: []):
        pass

    def get_user(self):
        user = self.factory_method()
        return user