from __future__ import annotations
from models.User import User


class Student(User):
    def __init__(self, user_id: int, fname: str, lname: str, email: str, username: str, password: str, image: bytes,
                 subjects: []):
        User.__init__(self, user_id, fname, lname, email, username, password, image, subjects)
