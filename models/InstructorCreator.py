# factory method concrete subject
from models.Creator import Creator
from models.Instructor import Instructor


class InstructorCreator(Creator):
    def factory_method(self, user_id: int, fname: str, lname: str, email: str, username: str, password: str,
                       image: bytes, subjects: []) -> Instructor:
        return Instructor(user_id, fname, lname, email, username, password, image, subjects)
