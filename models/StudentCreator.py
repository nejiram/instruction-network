# factory method concrete subject
from models.Creator import Creator
from models.Student import Student


class StudentCreator(Creator):
    def factory_method(self, user_id: int, fname: str, lname: str, email: str, username: str, password: str,
                       image: bytes, subjects: []) -> Student:
        return Student(user_id, fname, lname, email, username, password, image, subjects)
