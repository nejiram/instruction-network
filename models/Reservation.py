from __future__ import annotations
from models.Student import Student
from models.Appointment import Appointment


class Reservation:
    def __init__(self, reservation_id: int, appointment: Appointment, student: Student):
        self.__reservation_id = reservation_id
        self.__appointment = appointment
        self.__student = student

    def get_reservation_id(self) -> int:
        return self.__reservation_id

    def set_reservation_id(self, reservation_id) -> None:
        self.__reservation_id = reservation_id

    def get_appointment(self) -> Appointment:
        return self.__appointment

    def set_appointment(self, appointment: Appointment) -> None:
        self.__appointment = appointment

    def get_student(self) -> Student:
        return self.__student

    def set_student(self, student: Student) -> None:
        self.__student = student

    def sort_reservations(self, reservations_list):
        for i in range(len(reservations_list)):
            for j in range(i + 1, len(reservations_list)):
                a1 = reservations_list[i].get_appointment()
                a2 = reservations_list[j].get_appointment()
                day1, month1, year1 = self.date_to_dd_mm_yyyy(a1.get_date())
                day2, month2, year2 = self.date_to_dd_mm_yyyy(a2.get_date())
                if year1 == year2:  # ako je ista godina
                    if month1 == month2:  # ako je isti mjesec
                        if day1 == day2:  # ako je isti dan
                            stime1 = self.time_to_sec(a1.get_start_time())
                            stime2 = self.time_to_sec(a2.get_start_time())
                            if stime2 < stime1:  # mijenjam samo ako je a2 ranije
                                reservations_list[i], reservations_list[j] = reservations_list[j], reservations_list[i]
                        elif day2 < day1:  # ako nije isti dan
                            reservations_list[i], reservations_list[j] = reservations_list[j], reservations_list[i]
                    elif month2 < month1:  # ako nije isti mjesec
                        reservations_list[i], reservations_list[j] = reservations_list[j], reservations_list[i]
                elif year2 < year1:  # ako nije ista godina
                    reservations_list[i], reservations_list[j] = reservations_list[j], reservations_list[i]
        return reservations_list
