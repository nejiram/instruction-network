from __future__ import annotations
import hashlib, binascii, os
from models.Singleton import Singleton
from models.Instructor import Instructor
from models.Student import Student
import sqlite3
from numpy import unique
from models.Appointment import Appointment
from models.Reservation import Reservation
from datetime import *


class DatabaseConnection(metaclass=Singleton):
    __database = r"..\util\instruction_network_database.db"

    # konstruktor
    def __init__(self):
        self.__conn = None
        try:
            self.__conn = sqlite3.connect(self.__database)
            print("CONNECTED")
            self.create_all_tables()
            self.insert_ratings()
            self.insert_subjects()
            self.insert_locations()
        except sqlite3.Error as e:
            print(e)

    # pomocna funkcija za kreiranje jedne tabele:
    def create_table(self, query) -> None:
        try:
            cur = self.__conn.cursor()
            cur.execute(query)
        except sqlite3.Error as e:
            print(e)

    # kreira sve tabele u bazi
    def create_all_tables(self) -> None:
        create_instructors_table = """CREATE TABLE IF NOT EXISTS instructors (
                                        instructor_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                        first_name TEXT NOT NULL, 
                                        last_name TEXT NOT NULL, 
                                        email TEXT NOT NULL, 
                                        username TEXT NOT NULL, 
                                        password TEXT NOT NULL,
                                        image BLOB
                                        );"""

        create_students_table = """CREATE TABLE IF NOT EXISTS students (
                                    student_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                    first_name TEXT NOT NULL, 
                                    last_name TEXT NOT NULL, 
                                    email TEXT NOT NULL, 
                                    username TEXT NOT NULL, 
                                    password TEXT NOT NULL,
                                    image BLOB
                                    );"""

        create_ratings_table = """CREATE TABLE IF NOT EXISTS ratings (
                                    rating_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                    grade INTEGER NOT NULL
                                    );"""

        create_instructors_ratings_table = """CREATE TABLE IF NOT EXISTS instructors_ratings (
                                                rating_id INTEGER NOT NULL, 
                                                instructor_id INTEGER NOT NULL, 
                                                student_id INTEGER NOT NULL,
                                                FOREIGN KEY (rating_id) REFERENCES ratings (rating_id), 
                                                FOREIGN KEY (instructor_id) REFERENCES instructors (instructor_id), 
                                                FOREIGN KEY (student_id) REFERENCES students (student_id)
                                                );"""

        create_subjects_table = """CREATE TABLE IF NOT EXISTS subjects (
                                    subject_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                    subject_name TEXT NOT NULL);"""

        create_instructors_subjects_table = """CREATE TABLE IF NOT EXISTS instructors_subjects (
                                                instructor_id INTEGER NOT NULL, 
                                                subject_id INTEGER NOT NULL, 
                                                FOREIGN KEY (instructor_id) REFERENCES instructors (instructor_id),
                                                FOREIGN KEY (subject_id) REFERENCES subjects (subject_id)
                                                );"""

        create_students_subjects_table = """CREATE TABLE IF NOT EXISTS students_subjects (
                                            student_id INTEGER NOT NULL, 
                                            subject_id INTEGER NOT NULL, 
                                            FOREIGN KEY (student_id) REFERENCES students (student_id), 
                                            FOREIGN KEY (subject_id) REFERENCES subjects (subject_id)
                                            );"""

        create_locations_table = """CREATE TABLE IF NOT EXISTS locations (
                                        location_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                        location TEXT NOT NULL
                                        );"""

        create_appointments_table = """CREATE TABLE IF NOT EXISTS appointments (
                                        appointment_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                        instructor_id INTEGER NOT NULL, 
                                        subject_id INTEGER NOT NULL, 
                                        date TEXT NOT NULL, 
                                        start_time TEXT NOT NULL, 
                                        end_time TEXT NOT NULL, 
                                        is_online INTEGER NOT NULL, 
                                        location_id INTEGER NOT NULL,
                                        price INTEGER NOT NULL, 
                                        FOREIGN KEY (instructor_id) REFERENCES instructors (instructor_id), 
                                        FOREIGN KEY (subject_id) REFERENCES subjects (subject_id), 
                                        FOREIGN KEY (location_id) REFERENCES locations (location_id) 
                                        );"""

        create_reservations_table = """CREATE TABLE IF NOT EXISTS reservations (
                                            reservation_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                            student_id INTEGER NOT NULL, 
                                            appointment_id INTEGER NOT NULL, 
                                            FOREIGN KEY (student_id) REFERENCES students (student_id),
                                            FOREIGN KEY (appointment_id) REFERENCES appointments (appointment_id)
                                            );"""
        try:
            self.create_table(create_instructors_table)
            self.create_table(create_students_table)
            self.create_table(create_ratings_table)
            self.create_table(create_instructors_ratings_table)
            self.create_table(create_subjects_table)
            self.create_table(create_instructors_subjects_table)
            self.create_table(create_students_subjects_table)
            self.create_table(create_locations_table)
            self.create_table(create_appointments_table)
            self.create_table(create_reservations_table)
            self.__conn.commit()
        except sqlite3.Error as e:
            print(e)

    # dodaje predmete u bazu
    def insert_subjects(self) -> None:
        cur = self.__conn.cursor()
        subjects = ("C++", "Matematika", "Fizika", "C")

        for s in subjects:
            cur.execute("INSERT INTO subjects (subject_name) VALUES (:subject_name)", {"subject_name": s})

        self.__conn.commit()

    # dodaje ocjene u bazu
    def insert_ratings(self) -> None:
        cur = self.__conn.cursor()
        grades = (1, 2, 3, 4, 5)

        for g in grades:
            cur.execute("INSERT INTO ratings (grade) VALUES (:grade)", {"grade": g})

        self.__conn.commit()

    # dodaje lokacije u bazu
    def insert_locations(self) -> None:
        cur = self.__conn.cursor()
        locations = ("--", "Sarajevo", "Tuzla", "Zenica", "Mostar", "Banja Luka")

        for l in locations:
            cur.execute("INSERT INTO locations (location) VALUES (:location)", {"location": l})

        self.__conn.commit()

    # vraca predmete iz baze
    def get_subjects(self) -> []:
        cur = self.__conn.cursor()

        cur.execute("SELECT * FROM subjects")
        records = cur.fetchall()

        self.__conn.commit()
        return records

    # vraca nazive predmeta iz baze
    def get_subjects_names(self) -> ():
        cur = self.__conn.cursor()

        cur.execute("SELECT subject_name FROM subjects")
        records = cur.fetchall()

        self.__conn.commit()
        subject_names = []
        for sn in records:
            subject_names.append(sn[0])

        subject_names = tuple(subject_names)
        return subject_names

    # vraca ocjene iz baze
    def get_ratings(self) -> ():
        cur = self.__conn.cursor()

        cur.execute("SELECT grade FROM ratings")
        records = cur.fetchall()

        self.__conn.commit()
        ratings = []
        for grade in records:
            ratings.append(grade[0])

        ratings = tuple(ratings)
        return ratings

    # pomocna funkcija za uzimanje id predmeta na osnovu imena
    def get_subjects_id(self, subject_name: str) -> int:
        cur = self.__conn.cursor()
        cur.execute("SELECT subject_id FROM subjects WHERE subject_name LIKE ?", (subject_name,))
        r = cur.fetchall()
        self.__conn.commit()
        return r[0][0]

    # vraca lokacije iz baze
    def get_locations(self) -> ():
        cur = self.__conn.cursor()

        cur.execute("SELECT location FROM locations")
        rec = cur.fetchall()

        self.__conn.commit()
        locations = []
        for l in rec:
            locations.append(l[0])

        locations = tuple(locations)
        return locations

    # dodavanje novog instruktora
    def insert_into_instructors(self, instructor: Instructor) -> int:
        cur = self.__conn.cursor()

        cur.execute("SELECT * FROM instructors WHERE email LIKE ?", (instructor.get_email(),))
        emails_rec = cur.fetchall()

        cur.execute("SELECT * FROM instructors WHERE username LIKE ?", (instructor.get_username(),))
        instructor_usernames_rec = cur.fetchall()

        cur.execute("SELECT * FROM students WHERE username LIKE ?", (instructor.get_username(),))
        student_username_rec = cur.fetchall()

        if len(emails_rec) == 0 and len(instructor_usernames_rec) == 0 and len(student_username_rec) == 0:
            cur.execute("""INSERT INTO instructors (first_name, last_name, email, username, password, image) 
                            VALUES (:fname, :lname, :email, :username, :password, :image)""",
                        {
                            "fname": instructor.get_first_name(),
                            "lname": instructor.get_last_name(),
                            "email": instructor.get_email(),
                            "username": instructor.get_username(),
                            "password": self.hash_password(instructor.get_password()),
                            "image": instructor.get_image()
                        })
            id = cur.lastrowid

            subjects_id = []
            for s in instructor.get_subjects():
                subjects_id.append(self.get_subjects_id(s))

            for s_id in subjects_id:
                cur.execute("""INSERT INTO instructors_subjects (instructor_id, subject_id) 
                            VALUES (:instructor_id, :subject_id)""",
                            {
                                "instructor_id": id,
                                "subject_id": s_id
                            })

            self.__conn.commit()
            return id
        return -1

    # dodavanje novo studenta
    def insert_into_students(self, student: Student) -> int:
        cur = self.__conn.cursor()

        cur.execute("SELECT * FROM students WHERE email LIKE ?", (student.get_email(),))
        emails_rec = cur.fetchall()

        cur.execute("SELECT * FROM students WHERE username LIKE ?", (student.get_username(),))
        student_username_rec = cur.fetchall()

        cur.execute("SELECT * FROM instructors WHERE username LIKE ?", (student.get_username(),))
        instructor_usernames_rec = cur.fetchall()

        if len(emails_rec) == 0 and len(instructor_usernames_rec) == 0 and len(student_username_rec) == 0:
            cur.execute("""INSERT INTO students (first_name, last_name, email, username, password, image) 
                                        VALUES (:fname, :lname, :email, :username, :password, :image)""",
                        {
                            "fname": student.get_first_name(),
                            "lname": student.get_last_name(),
                            "email": student.get_email(),
                            "username": student.get_username(),
                            "password": self.hash_password(student.get_password()),
                            "image": student.get_image()
                        })
            id = cur.lastrowid

            subjects_id = []
            for s in student.get_subjects():
                subjects_id.append(self.get_subjects_id(s))

            for s_id in subjects_id:
                cur.execute("""INSERT INTO students_subjects (student_id, subject_id) 
                            VALUES (:student_id, :subject_id)""",
                            {
                                "student_id": id,
                                "subject_id": s_id
                            })
            self.__conn.commit()
            return id
        return -1

    # pomocna funkcija za uzimanje imena predmeta na osnovu id-a
    def get_subjects_name_by_id(self, subject_id: int) -> str:
        cur = self.__conn.cursor()
        cur.execute("SELECT subject_name FROM subjects WHERE subject_id=?", (subject_id,))
        subjects_rec = cur.fetchall()
        return subjects_rec[0][0]

    # pomocna funkcija za uzimanje instruktora na osnocu id-a
    def get_instructor_by_id(self, instructor_id: int) -> ():
        cur = self.__conn.cursor()

        cur.execute("SELECT * FROM instructors WHERE instructor_id=?", (instructor_id,))
        rec = cur.fetchall()
        return rec[0][1], rec[0][2], rec[0][3], rec[0][4], rec[0][5], rec[0][6]

    # pomocna funkcija za uzimanje instruktora koji imaju iste predmete kao student
    def get_matching_instructors(self, student_id: int) -> ():
        cur = self.__conn.cursor()

        # uzima sve id predmeta koje je uzeo student
        cur.execute("SELECT subject_id FROM students_subjects WHERE student_id=?", (student_id,))
        students_subjects_id = cur.fetchall()

        # uzima sve id instruktora koji odgovaraju studentovim predmetima
        instructors_id_rec = []
        for s_id in students_subjects_id:
            cur.execute("SELECT instructor_id FROM instructors_subjects WHERE subject_id=?", (s_id[0],))
            i = cur.fetchall()
            instructors_id_rec.append(i)

        instructors_id = []

        for i in instructors_id_rec:
            for id in i:
                instructors_id.append(id[0])

        instructors_id = unique(instructors_id)
        instructors_id = instructors_id.tolist()

        instructors = []
        for i in instructors_id:
            # uzima sve instruktore na osnovu id-a
            fname, lname, email, username, password, image = self.get_instructor_by_id(i)
            # uzima predmete koji odgovaraju instruktorima
            subjects = self.get_subjects_by_instructors_id(i)
            instr = Instructor(i, fname, lname, email, username, password, image, subjects)
            # dodjeljuje ocjene instruktoru
            instr.set_ratings(self.get_instructors_ratings(i))
            instructors.append(instr)

        self.__conn.commit()
        return tuple(instructors)

    # pomocna funkcija za uzimanje imena predmeta na osnovu id-a studenta
    def get_subjects_by_students_id(self, student_id: int) -> []:
        cur = self.__conn.cursor()

        cur.execute("SELECT subject_id FROM students_subjects WHERE student_id=?", (student_id,))
        subjects_id_rec = cur.fetchall()
        subjects = []
        for s_id in subjects_id_rec:
            subjects.append(self.get_subjects_name_by_id(s_id[0]))

        self.__conn.commit()
        return subjects

    # pomocna funkcija za uzimanje imena predmeta na osnovu id-a instruktora
    def get_subjects_by_instructors_id(self, instructor_id: int) -> []:
        cur = self.__conn.cursor()

        cur.execute("SELECT subject_id FROM instructors_subjects WHERE instructor_id=?", (instructor_id,))
        subjects_id_rec = cur.fetchall()
        subjects = []
        for s_id in subjects_id_rec:
            subjects.append(self.get_subjects_name_by_id(s_id[0]))

        self.__conn.commit()
        return subjects

    # uzimanje podataka za login
    def get_login_data(self, username: str, password: str) -> ():
        cur = self.__conn.cursor()

        # da li je instruktor
        cur.execute("SELECT * FROM instructors WHERE username LIKE ?", (username,))
        instructor_rec = cur.fetchall()

        # slucaj kada nije instruktor
        if len(instructor_rec) == 0:
            cur.execute("SELECT * FROM students WHERE username LIKE ?", (username,))
            student_rec = cur.fetchall()

            # ako nije ni student
            if len(student_rec) == 0:
                self.__conn.commit()
                return None

            # ako je student
            if self.verify_password(student_rec[0][5], password) is True:
                subjects = self.get_subjects_by_students_id(student_rec[0][0])
                tmp = student_rec[0]
                student = Student(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], subjects)
                self.__conn.commit()
                return "student", student
            return None
        elif self.verify_password(instructor_rec[0][5], password) is True:
            subjects = self.get_subjects_by_instructors_id(instructor_rec[0][0])
            tmp = instructor_rec[0]
            instructor = Instructor(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], subjects)
            instructor.set_ratings(self.get_instructors_ratings(tmp[0]))
            self.__conn.commit()
            return "instructor", instructor
        return None

    # brisanje studenta
    def delete_student(self, id: int) -> None:
        cur = self.__conn.cursor()

        # brisem iz student_subjects
        cur.execute("DELETE FROM students_subjects WHERE student_id=?", (id,))
        # brisem iz reservations
        cur.execute("DELETE FROM reservations WHERE student_id=?", (id,))
        # brisem iz instructors_ratings
        cur.execute("DELETE FROM instructors_ratings WHERE student_id=?", (id,))
        # brisem iz students
        cur.execute("DELETE FROM students WHERE student_id=?", (id,))

        self.__conn.commit()

    # promjena podataka - student
    def update_students_data(self, student: Student) -> None:
        cur = self.__conn.cursor()

        cur.execute("""UPDATE students SET 
                    first_name=:fname,
                    last_name=:lname,
                    email=:email,
                    username=:username,
                    password=:password,
                    image=:image 
                    WHERE student_id=:id""",
                    {
                        "fname": student.get_first_name(),
                        "lname": student.get_last_name(),
                        "email": student.get_email(),
                        "username": student.get_username(),
                        "password": student.get_password(),
                        "image": student.get_image(),
                        "id": student.get_user_id()
                    })

        cur.execute("DELETE FROM students_subjects WHERE student_id=?", (student.get_user_id(),))

        subjects_id = []
        for s in student.get_subjects():
            subjects_id.append(self.get_subjects_id(s))

        for s_id in subjects_id:
            cur.execute("""INSERT INTO students_subjects (student_id, subject_id) 
                        VALUES (:student_id, :subject_id)""",
                        {
                            "student_id": student.get_user_id(),
                            "subject_id": s_id
                        })

        self.__conn.commit()

    # pretraga instruktora po username-u
    def get_instructor_by_username(self, username: str) -> Instructor:
        cur = self.__conn.cursor()

        cur.execute("SELECT * FROM instructors WHERE username LIKE ?", (username,))
        instructor_rec = cur.fetchall()
        tmp1 = instructor_rec[0]

        subjects = self.get_subjects_by_instructors_id(tmp1[0])

        instructor = Instructor(tmp1[0], tmp1[1], tmp1[2], tmp1[3], tmp1[4], tmp1[5], tmp1[6], subjects)
        instructor.set_ratings(self.get_instructors_ratings(tmp1[0]))
        self.__conn.commit()
        return instructor

    # promjena podataka - instruktor
    def update_instructors_data(self, instructor: Instructor) -> None:
        cur = self.__conn.cursor()

        cur.execute("""UPDATE instructors SET 
                            first_name=:fname,
                            last_name=:lname,
                            email=:email,
                            username=:username,
                            password=:password,
                            image=:image 
                            WHERE instructor_id=:id""",
                    {
                        "fname": instructor.get_first_name(),
                        "lname": instructor.get_last_name(),
                        "email": instructor.get_email(),
                        "username": instructor.get_username(),
                        "password": instructor.get_password(),
                        "image": instructor.get_image(),
                        "id": instructor.get_user_id()
                    })

        cur.execute("DELETE FROM instructors_subjects WHERE instructor_id=?", (instructor.get_user_id(),))

        subjects_id = []
        for s in instructor.get_subjects():
            subjects_id.append(self.get_subjects_id(s))

        for s_id in subjects_id:
            cur.execute("""INSERT INTO instructors_subjects (instructor_id, subject_id) 
                                VALUES (:instructor_id, :subject_id)""",
                        {
                            "instructor_id": instructor.get_user_id(),
                            "subject_id": s_id
                        })

        self.__conn.commit()

    # brisanje instruktora
    def delete_instructor(self, id: id) -> None:
        cur = self.__conn.cursor()

        # brisem iz instructors_subjects
        cur.execute("DELETE FROM instructors_subjects WHERE instructor_id=?", (id,))
        # brisem iz instructors_ratings
        cur.execute("DELETE FROM instructors_ratings WHERE instructor_id=?", (id,))
        # brisem iz reservations
        cur.execute("DELETE FROM reservations WHERE instructor_id=?", (id,))
        # brisem iz appointments
        cur.execute("DELETE FROM appointments WHERE instructor_id=?", (id,))
        # brisem iz instructors
        cur.execute("DELETE FROM instructors WHERE instructor_id=?", (id,))

        self.__conn.commit()

    # pomocna funkcija za uzimanje lokacije na osnovu id-a
    def get_locations_name(self, location_id: int) -> str:
        cur = self.__conn.cursor()

        cur.execute("SELECT location FROM locations WHERE location_id=?", (location_id,))
        rec = cur.fetchall()

        self.__conn.commit()
        return rec[0][0]

    # pomocna funkcija za uzimanje id-a lokacije na osnovu imena
    def get_locations_id(self, location: str) -> int:
        cur = self.__conn.cursor()

        cur.execute("SELECT location_id FROM locations WHERE location LIKE ?", (location,))
        rec = cur.fetchall()

        self.__conn.commit()
        return rec[0][0]

    # uzimanje kreiranih termina
    def get_created_appointments(self, instructor_id: int) -> []:
        cur = self.__conn.cursor()

        cur.execute("SELECT * FROM appointments WHERE instructor_id=?", (instructor_id,))
        rec = cur.fetchall()

        appointments = []
        for r in rec:
            # uzimam instruktora
            fname, lname, email, username, password, image = self.get_instructor_by_id(r[1])
            subjects = self.get_subjects_by_instructors_id(r[1])
            instructor = Instructor(r[1], fname, lname, email, username, password, image, subjects)
            # uzimam predmet
            subject = self.get_subjects_name_by_id(r[2])
            # uzimam lokaciju
            location = self.get_locations_name(r[7])
            appointment = Appointment(r[0], instructor, subject, r[3], r[4], r[5], r[6], location, r[8])
            appointments.append(appointment)

        self.__conn.commit()
        return self.sort_appointments(appointments)

    # dodavanje termina
    def insert_into_appointments(self, appointment: Appointment) -> int:
        cur = self.__conn.cursor()

        # id instruktora
        instructor_id = appointment.get_instructor().get_user_id()

        # postojeci termini
        created_appointments = self.get_created_appointments(instructor_id)

        for a in created_appointments:
            if a.get_date() == appointment.get_date():
                start_time = self.time_to_sec(a.get_start_time())
                end_time = self.time_to_sec(a.get_end_time())
                new_start_time = self.time_to_sec(appointment.get_start_time())
                new_end_time = self.time_to_sec(appointment.get_end_time())
                if (start_time <= new_start_time <= end_time) or (
                        start_time <= new_end_time <= end_time) or (
                        new_start_time <= start_time and new_end_time >= end_time):
                    return -1

        # id predmeta
        subject_id = self.get_subjects_id(appointment.get_subject())

        # dodavanje termina
        cur.execute("""INSERT INTO appointments (instructor_id, subject_id, date, start_time, end_time, is_online, location_id, price) 
                    VALUES (:instructor_id, :subject_id, :date, :start_time, :end_time, :is_online, :location_id, :price)""",
                    {
                        "instructor_id": instructor_id,
                        "subject_id": subject_id,
                        "date": appointment.get_date(),
                        "start_time": appointment.get_start_time(),
                        "end_time": appointment.get_end_time(),
                        "is_online": appointment.get_is_online(),
                        "location_id": self.get_locations_id(appointment.get_location()),
                        "price": appointment.get_price()
                    })

        self.__conn.commit()
        return 1

    # uzimanje termina koji odgovaraju studentovim predmetima a koji nisu rezervisani
    def get_all_available_appointments(self, student_id: int, subjects: []) -> []:
        cur = self.__conn.cursor()

        instructors = self.get_matching_instructors(student_id)

        appointments = []
        for instr in instructors:
            cur.execute("SELECT * FROM appointments WHERE instructor_id=?", (instr.get_user_id(),))
            rec = cur.fetchall()
            for r in rec:
                # da li je termin prosao

                # datum i vrijeme rezervacije
                dd, mm, yyyy = self.date_to_dd_mm_yyyy(r[3])
                sec = self.time_to_sec(r[4])

                # trenutni datum i vrijeme
                day_now, month_now, year_now = self.get_todays_dd_mm_yyyy()
                sec_now = self.current_time_to_seconds()

                if (yyyy > year_now) or (yyyy == year_now and mm > month_now) or (
                        yyyy == year_now and mm == month_now and dd > day_now) or (
                        yyyy == year_now and mm == month_now and dd == day_now and sec > sec_now):
                    sub = self.get_subjects_name_by_id(r[2])
                    for s in subjects:
                        if s == sub:
                            location = self.get_locations_name(r[7])
                            a = Appointment(r[0], instr, sub, r[3], r[4], r[5], r[6], location, r[8])
                            appointments.append(a)

        # provjeravam da li je termin rezervisan
        available_appointments = []
        for a in appointments:
            cur.execute("SELECT * FROM reservations WHERE appointment_id=?", (a.get_appointment_id(),))
            rec = cur.fetchall()
            if len(rec) == 0:
                available_appointments.append(a)

        self.__conn.commit()
        return self.sort_appointments(available_appointments)

    # vracanje termina koji odgovaraju studentu za pojedinog instruktora
    def get_instructors_appointments(self, instructor_id: int, student_id: int, subjects: int) -> ():

        all_appointments = self.get_all_available_appointments(student_id, subjects)
        appointments = []
        for a in all_appointments:
            if a.get_instructor().get_user_id() == instructor_id:
                appointments.append(a)

        return self.sort_appointments(appointments)

    # dodavanje rezervacije
    def insert_into_reservations(self, student: Student, appointment: Appointment) -> int:
        cur = self.__conn.cursor()

        # uzimam vec rezervisane termine od studenta
        reservations = self.get_students_reservations(student)

        # provjeravam ima li vec rezervisan termin u isto vrijeme
        for r in reservations:
            a = r.get_appointment()
            if a.get_date() == appointment.get_date():
                start_time = self.time_to_sec(a.get_start_time())
                end_time = self.time_to_sec(a.get_end_time())
                new_start_time = self.time_to_sec(appointment.get_start_time())
                new_end_time = self.time_to_sec(appointment.get_end_time())
                if (start_time <= new_start_time <= end_time) or (
                        start_time <= new_end_time <= end_time) or (
                        new_start_time <= start_time and new_end_time >= end_time):
                    return -1

        # uzimam id predmeta
        subject_id = self.get_subjects_id(appointment.get_subject())

        # uzimam id termina
        cur.execute("""SELECT appointment_id FROM appointments WHERE instructor_id=? AND 
                    subject_id=? AND date LIKE ? AND start_time LIKE ? AND end_time LIKE ?""",
                    (appointment.get_instructor().get_user_id(), subject_id, appointment.get_date(),
                     appointment.get_start_time(), appointment.get_end_time(),))
        rec = cur.fetchall()
        appointment.set_appointment_id(rec[0][0])

        # dodajem u rezervacije
        cur.execute("""INSERT INTO reservations (student_id, appointment_id)
                    VALUES (:student_id, :appointment_id)""",
                    {
                        "student_id": student.get_user_id(),
                        "appointment_id": appointment.get_appointment_id()
                    })

        self.__conn.commit()
        return 1

    # vraca rezervacije za instruktora
    def get_instructors_reservations(self, instructor_id: int) -> []:
        cur = self.__conn.cursor()

        # termini koje je kreirao instruktor
        created_appoitnments = self.get_created_appointments(instructor_id)

        reservations = []
        for a in created_appoitnments:
            cur.execute("SELECT reservation_id, student_id FROM reservations WHERE appointment_id=?",
                        (a.get_appointment_id(),))
            rec = cur.fetchall()
            if len(rec) != 0:
                reservation_id = rec[0][0]
                student_id = rec[0][1]
                cur.execute("SELECT * FROM students WHERE student_id=?", (student_id,))
                student_rec = cur.fetchall()
                subjects = self.get_subjects_by_students_id(student_id)
                tmp = student_rec[0]
                student = Student(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], subjects)
                reservation = Reservation(reservation_id, a, student)
                reservations.append(reservation)

        self.__conn.commit()
        return self.sort_reservations(reservations)

    # vraca rezervacije za studente
    def get_students_reservations(self, student: Student):
        cur = self.__conn.cursor()

        cur.execute("SELECT * FROM reservations WHERE student_id=?", (student.get_user_id(),))
        reservations_rec = cur.fetchall()

        reservations = []
        for r in reservations_rec:
            cur.execute("SELECT * FROM appointments WHERE appointment_id=?", (r[2],))
            appointments_rec = cur.fetchall()
            fname, lname, email, username, password, image = self.get_instructor_by_id(appointments_rec[0][1])
            subjects = self.get_subjects_by_instructors_id(appointments_rec[0][1])
            instructor = Instructor(appointments_rec[0][1], fname, lname, email, username, password, image, subjects)
            subject_name = self.get_subjects_name_by_id(appointments_rec[0][2])
            location = self.get_locations_name(appointments_rec[0][7])
            tmp = appointments_rec[0]
            appointment = Appointment(tmp[0], instructor, subject_name, tmp[3], tmp[4], tmp[5], tmp[6], location,
                                      tmp[8])
            reservation = Reservation(r[0], appointment, student)
            reservations.append(reservation)

        self.__conn.commit()
        return self.sort_reservations(reservations)

    # otkazivanje rezervacije
    def delete_from_reservations(self, reservation: Reservation) -> int:
        cur = self.__conn.cursor()

        # datum i vrijeme rezervacije
        dd, mm, yyyy = self.date_to_dd_mm_yyyy(reservation.get_appointment().get_date())
        sec = self.time_to_sec(reservation.get_appointment().get_start_time())

        # trenutni datum i vrijeme
        day, month, year = self.get_todays_dd_mm_yyyy()
        now = self.current_time_to_seconds()

        # provjera da li je datum istekao ili je vrijeme termina unutar narednih 24h
        if year > yyyy or (year == yyyy and month > mm) or (year == yyyy and month == mm and day > dd):
            return 0
        elif year == yyyy and month == mm and day == dd:
            return -1
        elif year == yyyy and month == mm and day == dd - 1:
            if 86400 - abs(now - sec) < 0:
                return -1

        # brisem iz reservations
        cur.execute("DELETE FROM reservations WHERE reservation_id=?", (reservation.get_reservation_id(),))
        self.__conn.commit()

        return 1

    # provjerava da li je student vec ocijenio instruktora
    def check_ratings(self, instructor_id: int, student_id: int) -> int:
        cur = self.__conn.cursor()

        cur.execute("SELECT * FROM instructors_ratings WHERE instructor_id=? AND student_id=?",
                    (instructor_id, student_id,))
        rec = cur.fetchall()

        self.__conn.commit()
        if len(rec) == 0:
            return 1
        return -1

    # ocjenjivanje instruktora
    def rate_instructor(self, instructor_id: int, student_id: int, grade: int) -> int:
        cur = self.__conn.cursor()

        # provjeri da li je student već imao termin kod instruktora koji je prošao
        cur.execute("SELECT appointment_id FROM reservations WHERE student_id=?", (student_id,))
        appointments_id_rec = cur.fetchall()

        # student nema nijedan termin rezervisan
        if len(appointments_id_rec) == 0:
            return -1

        # id ocjene
        cur.execute("SELECT rating_id FROM ratings WHERE grade=?", (grade,))
        rating_id = cur.fetchall()[0][0]

        ok = True
        for a_id in appointments_id_rec:
            cur.execute("SELECT * FROM appointments WHERE appointment_id=? AND instructor_id=?",
                        (a_id[0], instructor_id,))
            appointments_rec = cur.fetchall()
            # student nema nijedan termin kod ovog instruktora
            if len(appointments_rec) == 0:
                ok = False
                continue
            # treba provjerit da li je proslo minimalno 24h od termina
            day, month, year = self.get_todays_dd_mm_yyyy()
            for a in appointments_rec:
                dd, mm, yyyy = self.date_to_dd_mm_yyyy(a[3])
                if year > yyyy or (year == year and month > mm) or (year == yyyy and month == mm and day - 1 > dd):
                    cur.execute("""INSERT INTO instructors_ratings (rating_id, instructor_id, student_id)
                                VALUES (:rating_id, :instructor_id, :student_id)""",
                                {
                                    "rating_id": rating_id,
                                    "instructor_id": instructor_id,
                                    "student_id": student_id
                                })
                    self.__conn.commit()
                    return 1
                elif year == yyyy and month == mm and day - 1 == dd:
                    now = self.current_time_to_seconds()
                    sec = self.time_to_sec(a[5])
                    if abs(now - sec) >= 86400:
                        cur.execute("""INSERT INTO instructors_ratings (rating_id, instructor_id, student_id)
                                                        VALUES (:rating_id, :instructor_id, :student_id)""",
                                    {
                                        "rating_id": rating_id,
                                        "instructor_id": instructor_id,
                                        "student_id": student_id
                                    })
                        self.__conn.commit()
                        return 1
                    else:
                        return -1

        self.__conn.commit()
        if ok is False:
            return 0
        return 1

    # promjena ocjene
    def change_grade(self, instructor_id, student_id, grade) -> None:
        cur = self.__conn.cursor()

        cur.execute("DELETE FROM instructors_ratings WHERE instructor_id=? AND student_id=?",
                    (instructor_id, student_id,))

        cur.execute("SELECT rating_id FROM ratings WHERE grade=?", (grade,))
        rating_id = cur.fetchall()[0][0]

        cur.execute("""INSERT INTO instructors_ratings (rating_id, instructor_id, student_id)
                                        VALUES (:rating_id, :instructor_id, :student_id)""",
                    {
                        "rating_id": rating_id,
                        "instructor_id": instructor_id,
                        "student_id": student_id
                    })

        self.__conn.commit()

    # uzimanje svih ocjena
    def get_instructors_ratings(self, instructor_id: int) -> []:
        cur = self.__conn.cursor()

        cur.execute("SELECT rating_id FROM instructors_ratings WHERE instructor_id=?", (instructor_id,))
        ratings_rec = cur.fetchall()

        ratings = []
        for r in ratings_rec:
            cur.execute("SELECT grade FROM ratings WHERE rating_id=?", (r[0],))
            grade = cur.fetchall()[0][0]
            ratings.append(grade)

        self.__conn.commit()
        return ratings

    # brisanje termina
    def delete_from_appointments(self, appointment_id: int):
        cur = self.__conn.cursor()

        cur.execute("DELETE FROM appointments WHERE appointment_id=?", (appointment_id,))

        self.__conn.commit()

    # destruktor
    def __del__(self):
        print("CONNECTION CLOSED")
        self.__conn.close()

    # pomocna funkcija za sortiranje termina
    def sort_appointments(self, appointments_list):
        for i in range(len(appointments_list)):
            for j in range(i + 1, len(appointments_list)):
                a1 = appointments_list[i]
                a2 = appointments_list[j]
                day1, month1, year1 = self.date_to_dd_mm_yyyy(a1.get_date())
                day2, month2, year2 = self.date_to_dd_mm_yyyy(a2.get_date())
                if year1 == year2:  # ako je ista godina
                    if month1 == month2:  # ako je isti mjesec
                        if day1 == day2:  # ako je isti dan
                            stime1 = self.time_to_sec(a1.get_start_time())
                            stime2 = self.time_to_sec(a2.get_start_time())
                            if stime2 < stime1:  # mijenjam samo ako je a2 ranije
                                appointments_list[i], appointments_list[j] = appointments_list[j], appointments_list[i]
                        elif day2 < day1:  # ako nije isti dan
                            appointments_list[i], appointments_list[j] = appointments_list[j], appointments_list[i]
                    elif month2 < month1:  # ako nije isti mjesec
                        appointments_list[i], appointments_list[j] = appointments_list[j], appointments_list[i]
                elif year2 < year1:  # ako nije ista godina
                    appointments_list[i], appointments_list[j] = appointments_list[j], appointments_list[i]
        return appointments_list

    # pomocna funkcija za sortiranje rezervacija
    def sort_reservations(self, reservations_list):
        for i in range(len(reservations_list)):
            for j in range(i + 1, len(reservations_list)):
                a1 = reservations_list[i].get_appointment()
                a2 = reservations_list[j].get_appointment()
                day1, month1, year1 = self.date_to_dd_mm_yyyy(a1.get_date())
                day2, month2, year2 = self.date_to_dd_mm_yyyy(a2.get_date())
                if year1 == year2:  # ako je ista godina
                    if month1 == month2:  # ako je isti mjesec
                        if day1 == day2:  # ako je isti dan
                            stime1 = self.time_to_sec(a1.get_start_time())
                            stime2 = self.time_to_sec(a2.get_start_time())
                            if stime2 < stime1:  # mijenjam samo ako je a2 ranije
                                reservations_list[i], reservations_list[j] = reservations_list[j], reservations_list[i]
                        elif day2 < day1:  # ako nije isti dan
                            reservations_list[i], reservations_list[j] = reservations_list[j], reservations_list[i]
                    elif month2 < month1:  # ako nije isti mjesec
                        reservations_list[i], reservations_list[j] = reservations_list[j], reservations_list[i]
                elif year2 < year1:  # ako nije ista godina
                    reservations_list[i], reservations_list[j] = reservations_list[j], reservations_list[i]
        return reservations_list

    # pomocna funkcija - vraca danasnji datum kao dan, mjesec i godinu
    def get_todays_dd_mm_yyyy(self) -> ():
        today = str(datetime.today())
        day = int(today[8:10])
        month = int(today[5:7])
        year = int(today[:4])
        return day, month, year

    # pomocna funkcija - pretvara trenutno vrijeme u sekunde
    def current_time_to_seconds(self) -> int:
        today = str(datetime.today())
        return int(today[11:13]) * 3600 + int(today[14:16]) * 60

    # pomocna funkcija - pretvara proslijedjeno vrijeme u sekunde
    def time_to_sec(self, t: str) -> int:
        return int(t[:2]) * 3600 + int(t[3:]) * 60

    # pomocna funkcija - pretvara proslijedjeni datum kao dan, mjesec i godinu
    def date_to_dd_mm_yyyy(self, d: str) -> ():
        yyyy = int(d[6:])
        mm = int(d[3:5])
        dd = int(d[:2])
        return dd, mm, yyyy

    # pomocna funkcija - hash-ira password
    def hash_password(self, password):
        """Hash a password for storing."""
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), salt, 100000)
        pwdhash = binascii.hexlify(pwdhash)
        return (salt + pwdhash).decode('ascii')

    # pomocna funkcija - provjerava da li je uneseni password jednak onom iz baze
    def verify_password(self, stored_password, provided_password):
        salt = stored_password[:64]
        stored_password = stored_password[64:]
        pwdhash = hashlib.pbkdf2_hmac('sha512', provided_password.encode('utf-8'), salt.encode('ascii'), 100000)
        pwdhash = binascii.hexlify(pwdhash).decode('ascii')
        return pwdhash == stored_password
