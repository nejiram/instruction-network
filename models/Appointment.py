from __future__ import annotations
from models.Instructor import Instructor


class Appointment:
    def __init__(self, appointment_id: int, instructor: Instructor, subject: str, date: str, start_time: str,
                 end_time: str, is_online: bool, location: str, price: int):
        self.__appointment_id = appointment_id
        self.instructor = instructor
        self.subject = subject
        self.__date = date
        self.__start_time = start_time
        self.__end_time = end_time
        self.__is_online = is_online
        self.__location = location
        self.__price = price

    def get_appointment_id(self) -> int:
        return self.__appointment_id

    def set_appointment_id(self, appointment_id: int) -> None:
        self.__appointment_id = appointment_id

    def get_instructor(self) -> Instructor:
        return self.instructor

    def set_instructor(self, instructor: Instructor) -> None:
        self.instructor = instructor

    def get_subject(self) -> str:
        return self.subject

    def set_subject(self, subject: str):
        self.subject = subject

    def get_date(self) -> str:
        return self.__date

    def set_date(self, date: str) -> None:
        self.__date = date

    def get_start_time(self) -> str:
        return self.__start_time

    def set_start_time(self, start_time: str) -> None:
        self.__start_time = start_time

    def get_end_time(self) -> str:
        return self.__end_time

    def set_end_time(self, end_time: str) -> None:
        self.__end_time = end_time

    def get_is_online(self) -> bool:
        return self.__is_online

    def set_is_online(self, is_online: bool):
        self.__is_online = is_online

    def get_location(self) -> str:
        return self.__location

    def set_location(self, location: str) -> None:
        self.__location = location

    def get_price(self) -> int:
        return self.__price

    def set_price(self, price: int) -> None:
        self.__price = price
