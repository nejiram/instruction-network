from __future__ import annotations
from abc import ABC


class User(ABC):
    def __init__(self, user_id: int, fname: str, lname: str, email: str, username: str, password: str, image: bytes,
                 subjects: []):
        self.__user_id = user_id
        self.__fname = fname
        self.__lname = lname
        self.__email = email
        self.__username = username
        self.__password = password
        self.__image = image
        self.__subjects = subjects

    def get_user_id(self) -> int:
        return self.__user_id

    def set_user_id(self, user_id: int) -> None:
        self.__user_id = user_id

    def get_first_name(self) -> str:
        return self.__fname

    def set_first_name(self, fname: str) -> None:
        self.__fname = fname

    def get_last_name(self) -> str:
        return self.__lname

    def set_last_name(self, lname: str) -> None:
        self.__lname = lname

    def get_email(self) -> str:
        return self.__email

    def set_email(self, email: str) -> None:
        self.__email = email

    def get_username(self) -> str:
        return self.__username

    def set_username(self, username: str) -> None:
        self.__username = username

    def get_password(self) -> str:
        return self.__password

    def set_password(self, password: str) -> None:
        self.__password = password

    def get_image(self) -> bytes:
        return self.__image

    def set_image(self, image: bytes) -> None:
        self.__image = image

    def get_subjects(self) -> []:
        return self.__subjects

    def set_subjects(self, subjects: []) -> None:
        self.__subjects = subjects

    def add_subject(self, subject: str) -> None:
        self.__subjects.extend(subject)

