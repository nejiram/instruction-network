from __future__ import annotations
from models.User import User


class Instructor(User):
    def __init__(self, user_id: int, fname: str, lname: str, email: str, username: str, password: str, image: bytes,
                 subjects: []):
        User.__init__(self, user_id, fname, lname, email, username, password, image, subjects)
        self.__ratings = []

    def get_ratings(self) -> []:
        return self.__ratings

    def get_average_rating(self) -> float:
        sum = 0
        if len(self.__ratings) != 0:
            for r in self.__ratings:
                sum = sum + r
            average_rating = sum / len(self.__ratings)
            return average_rating
        return 0.0

    def add_rating(self, rating: int) -> None:
        self.__ratings.append(rating)

    def set_ratings(self, ratings: []) -> None:
        self.__ratings = ratings
