import re
from tkinter import messagebox
from datetime import *


def validate_first_name(fname: str) -> bool:
    regex = "^[A-Z]{1}[a-z]*$"
    if re.search(regex, fname) is not None and len(fname) > 0:
        return True
    return False


def validate_last_name(lname: str) -> bool:
    regex = "^[A-Z]{1}[a-z]*$"
    if re.search(regex, lname) is not None and len(lname) > 0:
        return True
    return False


def validate_email(email: str) -> bool:
    regex = "^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$"
    if re.search(regex, email) is not None and len(email) > 0:
        return True
    return False


def validate_username(username: str) -> bool:
    regex = "^[a-zA-Z0-9_.-]+$"
    if re.search(regex, username) is not None and len(username) > 0:
        return True
    return False


def validate_password(password: str) -> bool:
    regex = "^[a-zA-Z0-9]+$"
    if re.search(regex, password) is not None or len(password) >= 8:
        return True
    return False


def validate_subjects(subjects: []) -> bool:
    for s in subjects:
        if s is not None:
            return True
    return False


def validate_users_data(fname: str, lname: str, email: str, username: str, password: str, subjects: []) -> bool:
    if validate_first_name(fname) is False:
        messagebox.showerror("Greška",
                             "Ime može sadržavati samo slova i prvo slovo imena mora biti veliko slovo.")
        return False
    elif validate_last_name(lname) is False:
        messagebox.showerror("Greška",
                             "Prezime može sadržavati samo slova i prvo slovo imena mora biti veliko slovo.")
        return False
    elif validate_email(email) is False:
        messagebox.showerror("Greška", "Pogrešan format email-a!")
        return False
    elif validate_username(username) is False:
        messagebox.showinfo("Greška",
                            "Username može sadržavati samo slova, brojeve i znakove: '-', '.', '_'.")
        return False
    elif validate_password(password) is False:
        messagebox.showinfo("Greška",
                            "Password može sadržavati samo velika i mala slova i brojeve, i mora imati najmanje 8 znakova.")
        return False
    elif validate_subjects(subjects) is False:
        messagebox.showerror("Greška", "Barem jedan predmet mora biti izabran.")
        return False
    return True


def validate_appointment_subject(subject: tuple) -> bool:
    if not subject:
        return False
    return True


def validate_date(date: str) -> bool:
    regex = "^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"
    if re.search(regex, date) is not None:
        return True
    return False


def validate_start_time(time: str) -> bool:
    regex = "^(([01]\d|2[0-3]):([0-5]\d)|24:00)$"
    if re.search(regex, time) is not None:
        return True
    return False


def validate_end_time(time: str) -> bool:
    regex = "^(([01]\d|2[0-3]):([0-5]\d)|24:00)$"
    if re.search(regex, time) is not None:
        return True
    return False


def validate_price(price: str) -> bool:
    regex = "^[0-9]*$"
    if re.search(regex, price) is not None:
        return True
    return False


def validate_appointment(subject: tuple, date: str, start_time: str, end_time: str, is_online: bool, location: str,
                         price: str) -> bool:
    if len(date) == 0 or len(start_time) == 0 or len(end_time) == 0 or len(price) == 0:
        messagebox.showerror("Greška", "Sva polja moraju biti popunjena.")
        return False
    elif validate_appointment_subject(subject) is False:
        messagebox.showerror("Greška", "Morate selektovati predmet!")
        return False
    elif validate_date(date) is False:
        messagebox.showerror("Greška",
                             "Datum mora biti u formatu \"dd.mm.yyyy\" ili \"dd/mm/yyyy\" ili \"dd-mm-yyyy\" i "
                             "mora biti nakon tekućeg datuma.")
        return False
    elif int(date[6:]) < int(datetime.today().strftime("%d/%m/%Y")[6:]) or (
            int(date[6:]) == int(datetime.today().strftime("%d/%m/%Y")[6:]) and int(date[3:5]) < int(
        datetime.today().strftime("%d/%m/%Y")[3:5])) or (
            int(date[6:]) == int(datetime.today().strftime("%d/%m/%Y")[6:]) and int(date[3:5]) == int(
        datetime.today().strftime("%d/%m/%Y")[3:5]) and int(date[:2]) <= int(
        datetime.today().strftime("%d/%m/%Y")[:2])):
        messagebox.showerror("Greška", "Datum termina mora biti nakon tekućeg datuma.")
        return False
    elif validate_start_time(start_time) is False:
        messagebox.showerror("Greška", "Vrijeme početka termina mora biti u formatu \"hh:mm\".")
        return False
    elif int(datetime.today().strftime("%d/%m/%Y")[:2]) + 1 == int(date[:2]):
        if int(start_time[:2]) < int(datetime.now().strftime("%d/%m/%Y %H:%M:%S")[11:13]) or (
                int(start_time[:2]) == int(datetime.now().strftime("%d/%m/%Y %H:%M:%S")[11:13]) and int(
                start_time[3:]) < int(datetime.now().strftime("%d/%m/%Y %H:%M:%S")[14:16])):
            messagebox.showerror("Greška", "Vrijeme početka termina može biti najranije za 24 sata.")
            return False
    elif validate_end_time(end_time) is False:
        messagebox.showerror("Greška", "Vrijeme kraja termina mora biti u formatu \"hh:mm\".")
        return False
    elif int(end_time[:2]) < int(start_time[:2]) or (
            int(end_time[:2]) == int(start_time[:2]) and int(end_time[3:]) <= int(start_time[3:])):
        messagebox.showerror("Greška", "Vrijeme kraja termina mora biti nakon vremena početka termina.")
        return False
    elif is_online is False and location == "--" or is_online is True and location != "--":
        messagebox.showerror("Greška", "Termin mora biti ili označen kao \"online\" ili imati lokaciju.")
        return False
    elif validate_price(price) is False:
        messagebox.showerror("Greška", "Cijena mora biti broj.")
        return False
    return True
